<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
<body>

	<form action="Search" method="post">
	
		<textarea type="text" name="json" cols="88" rows="10">
{"fields":[
{"name":"searchInput","value":"risotto panna"},
{"name":"difficulty","value":"bassa media"}
]}</textarea>

		<input type="submit" value="Send Json">
		
	</form>


<!-- 
		<textarea type="text" name="json" cols="88" rows="10">
{"fields":[
{"name":"title","value":"risotto","operator":"true","length":"0"},
{"name":"difficulty","value":"bassa","operator":"true","length":"0"},
{"name":"ingredient","value":"sale","operator":"true","length":"0"}
]}</textarea>
 -->

<!-- 
	<form action="servlet/test.ServletDemo" method="post">
		<input type="text" name="name">
		<input type="text" name="value">
		<input type="text" name="operator">
		<input type="text" name="length">
		<input type="submit" value="Send Query String">
		<br>
	</form>
	
	<form action="servlet/test.ServletDemo" method="post">
		<textarea type="text" name="json" cols="88" rows="10">
{"fields":[
{"name":"name1","value":"value1","operator":"operator1","length":"length1"},
{"name":"name2","value":"value2","operator":"operator2","length":"length2"}
]}
		</textarea>
		<input type="submit" value="Send Json">
	</form>
	
	<form action="servlet/test.ServletDemo" method="post">
		<textarea type="text" name="json-simple" cols="88" rows="10">
{"name": "name1","value": "value1","operator": "operator1","length": "length1"}
		</textarea>
		<input type="submit" value="Send Json">
	</form>
 -->
</body> 
</html>