define([], function () {

	String.prototype.replaceAll = function(token, newToken, ignoreCase) {
	    var str, i = -1, _token;
	    if((str = this.toString()) && typeof token === "string") {
	        _token = ignoreCase === true? token.toLowerCase() : undefined;
	        while((i = (
	            _token !== undefined? 
	                str.toLowerCase().indexOf(
	                            _token, 
	                            i >= 0? i + newToken.length : 0
	                ) : str.indexOf(
	                            token,
	                            i >= 0? i + newToken.length : 0
	                )
	        )) !== -1 ) {
	            str = str.substring(0, i)
	                    .concat(newToken)
	                    .concat(str.substring(i + token.length));
	        }
	    }
	return str;
	};
	
	var updateRecipeWithVisualInfos = function(data) {
		var maxScore = 0;
		var minScore = 1000;
		
		data.forEach( function (item, idx) {
			if (item.score > maxScore) {
				maxScore = item.score;
			}
			if (item.score < minScore) {
				minScore = item.score;
			}
		});
		
		var ratio = (parseFloat(minScore)/maxScore);
		console.log( "rapporto: " + ratio );
		if( ratio < 0.5 ) { ratio = 1 - ratio; }
		console.log( "rapporto: " + ratio );
		ratio = 1;
		//calcolo width e height 
		data.forEach( function (item, idx) {
			var score = Math.floor( (item.score / maxScore)*10 )/10;
			(score < 0.2) ? score = 0.2 : null;
			//console.log(score);
			//console.log(ratio);
			item.width = score * 420 * ratio;
			item.height = score * 300 * ratio;
			
			//console.log( "score: " + score );
			
			if (score >= 0.7) item.itemClass = 'imgBookmark-green';
			else if (score >= 0.3) item.itemClass = 'imgBookmark-yellow';
			else if (score > 0) item.itemClass = 'imgBookmark-red';
			
		});
		
		return data;
	}
	
	var fixImgSrc = function(data) {
		data.forEach( function (item, idx) {
			item.summary = item.summary.replaceAll("src=\"/", "src=\"http://www.giallozafferano.it/");
			item.preparation = item.preparation.replaceAll("src=\"/", "src=\"http://www.giallozafferano.it/");
		});	
		
		return data;
	}
	
	var truncateStr = function (str, len) {
		 if (str.length > len) {
 	        var new_str = str.substr ( 0, len+1 );

 	        while ( new_str.length )
 	        {
 	            var ch = new_str.substr ( -1 );
 	            new_str = new_str.substr ( 0, -1 );

 	            if ( ch == ' ' )
 	            {
 	                break;
 	            }
 	        }

 	        if ( new_str == '' )
 	        {
 	            new_str = str.substr ( 0, len );
 	        }

 	        return new_str +'...' ; 
 	    }
 	    return str;
	}
	
	var addPopoverSummary = function (data) {
		data.forEach( function (item, idx) {
			item.popoverSummary = truncateStr(item.summary, 100).replace(/\?/g, '\'');
		});	
		return data;		
	}
	
	
    return {
        'updateRecipeWithVisualInfos' : updateRecipeWithVisualInfos,
        'fixImgSrc' : fixImgSrc,
        'addPopoverSummary' : addPopoverSummary
    }
});
