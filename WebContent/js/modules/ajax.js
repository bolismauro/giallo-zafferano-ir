define(["jquery", "./config"], function ($, config) {
	
	/**
		1. Trova info
		2. Fa chiamata ajax per ottenere info sulle ricette
		3. Torna promise
	**/
	var getRecipeList = function (prepTimeDict, coTime, numOfPeopleDict, priceDict, diffDict, advancedSearchOn) {
		// get information from dom
		var data = {
			'searchInput' : $('#searchInput').val()
		};

		if (advancedSearchOn) {
			
			var difficulty = [];
			
			//difficolty
			$(".difficultyCheckBox:checked").each(function(idx, item){ 
				difficulty.push($(item).val());
			});

			data.difficulty = diffDict['min']+"-"+diffDict['max'];

			// ingredients
			var ingredients = [];

			$('.ingredient').each(function(idx, item){
				var name = $('.inputIngredientName', item).val();
				var qty = $('.inputIngredientQty', item).val();
				
				if( name == "" ) return;
				
				ingredients.push({
					'name' : name,
					'value' : qty
				});
			});
			data.ingredients = ingredients;

			// tempo preparazione			
			data.prepareTime = prepTimeDict['min']+"-"+prepTimeDict['max'];

			//  price
			data.price = priceDict['min']+"-"+priceDict['max'];

			// num people
			data.numberOfPeople = numOfPeopleDict['min']+"-"+numOfPeopleDict['max'];
			
			// tempo di cottura
			data.cookTime = coTime['min']+"-"+coTime['max'];
		}
		
		// likes
		var likes = JSON.parse(localStorage.getItem("likes"));
		if( typeof(likes) == "number" ) {
			var tmpLikes = [];
			tmpLikes.push(likes);
			data.likes = tmpLikes;
		}
		else {
			data.likes = JSON.parse(localStorage.getItem("likes"));
		}
		
		data = JSON.stringify(data);
		
		console.log("data: " + data);
		
		var promise = $.post(config.GET_RECIPE_URL, {
		  json: data,
		});

		return promise;
	};
	
	var getRecipeListFromIngredient = function (ingredients) {
		// get information from dom
		var data = {
			'ingredients' : [],
			'maxResults' : 50
		};
		
		ingredients.forEach(function (item) {
			data.ingredients.push({
				'name' : item,
				'value' : ''
			});
		});
		
		data = JSON.stringify(data);
		console.log(data);
		
		var promise = $.post(config.GET_RECIPE_URL, {
		  json: data,
		});

		return promise;		
	};
	
	
	var getRecipeListFromCategory = function (category) {
		var data = {
			'category' : category,
			'maxResults' : 50
		};		
		
		data = JSON.stringify(data);
		console.log(data);
		
		var promise = $.post(config.GET_RECIPE_URL, {
		  json: data,
		});

		return promise;			
	}


    return {
        getRecipeList: getRecipeList,
        getRecipeListFromIngredient : getRecipeListFromIngredient,
        getRecipeListFromCategory : getRecipeListFromCategory
    }
});