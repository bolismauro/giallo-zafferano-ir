define(["jquery", "modules/ajax", "modules/utils", "modules/config"], function ($, ajaxManager, utils, config) {
	
	var init = function(){
		

		var prepTime = {};
		var numOfPeople = {};
		var coTime = {};
		var price = {};
		var difficulty = {};
		var advancedSearchOn = false;
		
		var img = ["icon-coffee", "icon-food", "icon-beer", "icon-food"];
		var r = Math.floor(Math.random()*img.length);
		console.log( "random: " + r + " | img: " + img[r] );
		$(".flatware").addClass( img[r] );

		$('#toggleAdvancedSearch').on('click', function(){
			$('#advancedSearchDiv').toggle();
			advancedSearchOn = !advancedSearchOn;
		});

		$('#addIngredient').on('click', function(){
			$('#ingredientsFieldset').append('<div class="ingredient"> <label>Nome:</label> <input type="text" placeholder="Nome ingrediente" class="inputIngredientName"> &nbsp; <label>Quantità:</label> <input type="text" placeholder="Quantità" class="inputIngredientQty"></div>');
		});
		
		
		//suggerimento utente
		$('.searchBar').typeahead({
		    source: function (query, process) {
		    	
		    	var items = query.split(" ");
		    	query = items[items.length-1];
		    	
		    	//console.log(query);
		    	
		    	if (query.length < 3) {
		    		return [];
		    	}
		    	
		    	/*if (localStorage.getItem(query) !== null ){
		    		//console.log("cached version");
		    		return process(JSON.parse(localStorage.getItem(query)));
		    	}*/
		    	
		        return $.post(config.GET_TYPEAHEAD_URL, { query: query }, function (data) {
		        	//console.log(data);
		        	//localStorage.setItem(query, JSON.stringify(data));
		            return process(data);
		        });
		    }
		});

		$("#sliderDifficulty").slider({
			  from: 0,
			  to: 4,
			  scale: ['Molto Basso',  'Basso', 'Medio', 'Elevato', 'Molto Elevato'],
			  limits: true,
			  step: 1,
			  dimension: '',
			  skin : 'round_plastic',
			  'onstatechange' : function(value){
			  	var res = value.split(";");
			  	difficulty = {
			  		'min' : res[0],
			  		'max' : res[1]
			  	};
			  },
			  'calculate' : function(value){
				  if(value == 0) return 'Molto Basso';
				  if(value == 1) return 'Basso';
				  if(value == 2) return 'Medio';
				  if(value == 3) return 'Elavato';
				  if(value == 4) return 'Molto Elevato';
				  return '';
			  }
		});				
		
		// sliders
		// http://egorkhmelev.github.com/jslider/
		$("#sliderPreparationTime").slider({
		  from: 0,
		  to: 180,
		  scale: [0, '|', 60, '|' , 120, '|', 180],
		  limits: true,
		  step: 1,
		  dimension: '&nbsp;minuti',
		  skin : 'round_plastic',
		  'onstatechange' : function(value){
		  	var res = value.split(";");
		  	prepTime = {
		  		'min' : res[0],
		  		'max' : res[1]
		  	};
		  }
		});

		$("#sliderCookingTime").slider({
			  from: 0,
			  to: 180,
			  scale: [0, '|', 60, '|' , 120, '|', 180],
			  limits: true,
			  step: 1,
			  dimension: '&nbsp;minuti',
			  skin : 'round_plastic',
			  'onstatechange' : function(value){
			  	var res = value.split(";");
			  	coTime = {
			  		'min' : res[0],
			  		'max' : res[1]
			  	};
			  }
		});

		$("#sliderNumOfPeople").slider({
		  from: 0,
		  to: 10,
		  limits: true,
		  step: 1,
		  dimension: '&nbsp;persone',
		  skin : 'round_plastic',
		  'onstatechange' : function(value){
		  	var res = value.split(";");
		  	numOfPeople = {
		  		'min' : res[0],
		  		'max' : res[1]
		  	};
		  }
		});

		$("#sliderPrice").slider({
			  from: 0,
			  to: 4,
			  scale: ['Molto Basso',  'Basso', 'Medio', 'Elevato', 'Molto Elevato'],
			  limits: true,
			  step: 1,
			  dimension: '',
			  skin : 'round_plastic',
			  'onstatechange' : function(value){
			  	var res = value.split(";");
			  	price = {
			  		'min' : res[0],
			  		'max' : res[1]
			  	};
			  },
			  'calculate' : function(value){
				  if(value == 0) return 'Molto Basso';
				  if(value == 1) return 'Basso';
				  if(value == 2) return 'Medio';
				  if(value == 3) return 'Elavato';
				  if(value == 4) return 'Molto Elevato';
				  return '';
			  }
			});		
		

		$('.doSearch').on('click', function(){

			var searchString = $('#searchInput').val();
			

			ajaxManager.getRecipeList(prepTime, coTime, numOfPeople, price, difficulty, advancedSearchOn)
				.success( function (data) { 
					if(typeof data === "string"){
						data = JSON.parse(data);
					}
					
					data = utils.updateRecipeWithVisualInfos(data);
					data = utils.fixImgSrc(data);
					data = utils.addPopoverSummary(data);
					
					if (searchString === "") {
						likesMode = true;
					} else {
						likesMode = false;
					}
					
					console.log(data);
					//ajax call done, lets render the list
					require(["lib/text!../templates/list.html", "secondPage", "lib/Handlebars"], function(template, secondPage, Handlebars){
						var ctx = {
							"recipes" : data,
							"searchString" : searchString,
							'likesMode' : likesMode
						};
						var renderedHtml = Handlebars.compile(template)(ctx);
						$('#content').html(renderedHtml);
						secondPage.init(data);
						
					});
				})
    			
    			.error( function (err, erType) { 
    				alert("Errore nella richiesta");
    				console.log(erType);
    				return;
    			})
		});
	}

    return {
        init: init
    }
});