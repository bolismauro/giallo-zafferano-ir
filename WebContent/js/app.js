require(["jquery", "lib/Handlebars"], function($, Handlebars) {


	require(["lib/text!../templates/main.html", "firstPage"], function(template, firstPage){
		var template = Handlebars.compile(template);
		$('#content').html(template());
		firstPage.init();
	});



});
