define(["jquery", "modules/ajax", "modules/utils", "modules/config"], function ($, ajaxManager, utils, config) {
	
	var _getRecipeFormHash = function (recipes, hash) {
		
		var selectedRecipe = undefined;

		recipes.forEach( function (elm, idx) {
			// str e int comparison
			if (elm.recipeHash == hash) {
				selectedRecipe =  elm;
			}
		});
		return selectedRecipe;
	};

	
	var init = function (recipeData) {
		
		var img = ["icon-coffee", "icon-food", "icon-beer", "icon-food"];
		var r = Math.floor(Math.random()*img.length);
		console.log( "random: " + r + " | img: " + img[r] );
		$(".flatware").addClass( img[r] );
		
		$('.recipeThumb').popover();
		
		//suggerimento utente
		$('.searchBar').typeahead({
		    source: function (query, process) {
		    	
		    	var items = query.split(" ");
		    	query = items[items.length-1];
		    	
		    	//console.log(query);
		    	
		    	if (query.length < 2) {
		    		return [];
		    	}
		    	
		    	/*if (localStorage.getItem(query) !== null ){
		    		//console.log("json: " + JSON.parse(localStorage.getItem(query)));
		    		return process(JSON.parse(localStorage.getItem(query)));
		    	}*/
		    	
		        return $.post(config.GET_TYPEAHEAD_URL, { query: query }, function (data) {
		        	//console.log(data);
		        	//localStorage.setItem(query, JSON.stringify(data));
		            return process(data);
		        });
		    }
		});			
		

		
		$('#imagesContainer').freetile({
		    selector: '.imageContainer',
		    scoreFunction: function(o) {
		        return - 8* (o.TestedTop) - (o.TestedLeft);
		    }
		});


		$("img.lazy").lazyload({ 
		    effect : "fadeIn",
		    skip_invisible : false,
		    event : 'showThumbs',
		    effectspeed: 1000 
		});				
		
		setTimeout(function() { 
			$("img").trigger("showThumbs") 
		}, 100);
		
		$('.recipeThumb').on('click', function(elm){
			
			var hash = $(this).attr("data-hash");

			var recipe = _getRecipeFormHash(recipeData, hash);

			if (recipe === undefined) {
				alert("Si � verificato un errore");
				return;
			}
			
			require(["lib/text!../templates/modal.html", "lib/Handlebars"], function(template, Handlebars){
				
				recipe.summary = recipe.summary.replace(/\?/g, '\'');
				recipe.preparation = recipe.preparation.replace(/\?/g, '\'');
				
				//verifica dei like
				
				var likes = JSON.parse(localStorage.getItem("likes"));
				if (likes == null || likes == undefined) {
					recipe.like = false;
				} else if( typeof(likes) == "number" && likes == recipe.recipeHash ) {
					recipe.like = true;
				} else if( typeof(likes) != "number"  && likes.indexOf(recipe.recipeHash) >= 0 ) {
					recipe.like = true;
				} else {
					recipe.like = false;
				}
				console.log( "recipehash: " + recipe.recipeHash + " | like: " + recipe.like );
				
				var ctx = {
					"recipe" : recipe
				};

				console.log(ctx);

				var renderedHtml = Handlebars.compile(template)(ctx);
				
				$('#recipeModal').html(renderedHtml);
				
				$('#recipeModal').modal({
					'show': true
				});
				
				$('.ingLink').on('click', function () {
					var ingredient = $(this).html().toLowerCase();
					ajaxManager.getRecipeListFromIngredient([ingredient])
					.success( function (data) { 
						if(typeof data === "string"){
							data = JSON.parse(data);
						}
						console.log(data);
						
						data = utils.updateRecipeWithVisualInfos(data);				
						data = utils.fixImgSrc(data);
						data = utils.addPopoverSummary(data);
						
						$('#recipeModal').modal('hide');
						
						//ajax call done, lets render the list
						require(["lib/text!../templates/list.html", "secondPage", "lib/Handlebars"], function(template, secondPage, Handlebars){
							var ctx = {
								"recipes" : data,
								"searchString" :  ''
							};
							var renderedHtml = Handlebars.compile(template)(ctx);
							$('#content').html(renderedHtml);
							secondPage.init(data);
						});
					})
	    			
	    			.error( function (err, erType) { 
	    				alert("Errore nella richiesta");
	    				console.log(erType);
	    				return;
	    			})
				});
				
				$('.catLink').on('click', function () {
					var category = $(this).html().toLowerCase();
					ajaxManager.getRecipeListFromCategory(category)
					.success( function (data) { 
						if(typeof data === "string"){
							data = JSON.parse(data);
						}
						console.log(data);
						
						data = utils.updateRecipeWithVisualInfos(data);				
						data = utils.fixImgSrc(data);
						data = utils.addPopoverSummary(data);
						
						$('#recipeModal').modal('hide');
						
						//ajax call done, lets render the list
						require(["lib/text!../templates/list.html", "secondPage", "lib/Handlebars"], function(template, secondPage, Handlebars){
							var ctx = {
								"recipes" : data,
								"searchString" :  category
							};
							var renderedHtml = Handlebars.compile(template)(ctx);
							$('#content').html(renderedHtml);
							secondPage.init(data);
						});
					})
	    			
	    			.error( function (err, erType) { 
	    				alert("Errore nella richiesta");
	    				console.log(erType);
	    				return;
	    			})
				});
				
				// gestione dei like
				$("#like").click(function( data ){
					var likes = [];
					//console.log( $(this).data("recipehash") );
					var recipeHash = $(this).data("recipehash"); 
					likes = JSON.parse(localStorage.getItem("likes"));
					if( likes == null || likes == undefined ) {
						//var tmpLikes = [];
						//tmpLikes.push(0);
						//tmpLikes.push(recipeHash);
						//localStorage.setItem("likes", tmpLikes );
						localStorage.setItem("likes", recipeHash );
					} else if( typeof(likes) == "number" && recipeHash != likes ) {
						var tmpLikes = [];
						tmpLikes.push(likes);
						tmpLikes.push(recipeHash);
						localStorage.setItem("likes", JSON.stringify(tmpLikes) );
					} else if( typeof(likes) != "number" && likes.indexOf(recipeHash) < 0 ) {
						likes.push(recipeHash);
						localStorage.setItem("likes", JSON.stringify(likes) );
					}
					//$(this).css("color","#4d8cc6");
					$(this).addClass("thumbs-on");
					//console.log( "likes: " + JSON.parse(localStorage.getItem("likes")) );
				});
				
			});
			
		});


		$('#backToAdvancedSearch').on('click', function(){
			require(["lib/text!../templates/main.html", "firstPage", "lib/Handlebars"], function(template, firstPage, Handlebars){
				var template = Handlebars.compile(template);
				$('#content').html(template());
				firstPage.init();
				$('#toggleAdvancedSearch').click();
			});
		})
		
		$('.doSearch').on('click', function(){
			
			var searchString = $('#searchInput').val();
			

			ajaxManager.getRecipeList(undefined, undefined, undefined, undefined, undefined, false)
				.success( function (data) { 
					if(typeof data === "string"){
						data = JSON.parse(data);
					}
					console.log(data);
					
					data = utils.updateRecipeWithVisualInfos(data);				
					data = utils.fixImgSrc(data);
					data = utils.addPopoverSummary(data);
					
					if (searchString === "") {
						likesMode = true;
					} else {
						likesMode = false;
					}
					
					//ajax call done, lets render the list
					require(["lib/text!../templates/list.html", "secondPage", "lib/Handlebars"], function(template, secondPage, Handlebars){
						var ctx = {
							"recipes" : data,
							"searchString" : searchString || '',
							'likesMode' : likesMode
						};
						var renderedHtml = Handlebars.compile(template)(ctx);
						$('#content').html(renderedHtml);
						secondPage.init(data);
					});
				})
    			
    			.error( function (err, erType) { 
    				alert("Errore nella richiesta");
    				console.log(erType);
    				return;
    			})
		});
		
	}

    return {
        init: init
    }
});
