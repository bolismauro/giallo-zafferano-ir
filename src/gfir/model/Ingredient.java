package gfir.model;

import java.net.URI;

public class Ingredient {
	
	
	/**
	 * nome dell'ingrediente
	 */
	private String name;
	/**
	 * quantita' necessaria per la ricetta
	 */
	private String amount;
	/**
	 * collegamento alla pagina relativa all'ingrediente per maggiori informazioni
	 */
	private URI url;
	
	
	
	
	public Ingredient() {
		super();
	}


	/**
	 * costruttore della classe
	 * @param name
	 * @param amount
	 * @param url
	 */
	public Ingredient(String name, String amount, URI url) {
		super();
		this.url = url;
		this.name = name;
		this.amount = amount;
	}


	/**
	 * @return nome dell'ingrediente
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name nome dell'ingrediente
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return quantita' dell'ingrediente nella ricetta
	 */
	public String getAmount() {
		return amount;
	}


	/**
	 * @param amount quantita' dell'ingrediente nella ricetta
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}


	/**
	 * @return url della pagina dell'ingrediente nella ricetta
	 */
	public URI getUrl() {
		return url;
	}


	/**
	 * @param url url della pagina dell'ingrediente nella ricetta
	 */
	public void setUrl(URI url) {
		this.url = url;
	}
	
}
