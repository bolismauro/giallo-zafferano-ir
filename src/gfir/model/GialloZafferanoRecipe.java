package gfir.model;

import gfir.utils.IRLogger;
import gfir.utils.Utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GialloZafferanoRecipe extends Recipe{
	
	
	/**
	 * costruttore di default ereditato dalla superclasse
	 */
	public GialloZafferanoRecipe() {
		super();
	}
	
	/**
	 * costruttore ereditato dalla superclasse
	 * @param htmlPage
	 * @throws URISyntaxException 
	 */
	public GialloZafferanoRecipe(Document htmlPage) {
		super(htmlPage);
		this.analyzePage(htmlPage);
	}

	@Override
	public void analyzePage(Document htmlPage) {
		this.setPrepareTime( this.getPrepareTimeFromHtml(htmlPage) );
		this.setNumberOfPeople( this.getNumberOfPeopleFromHtml(htmlPage) );
		this.setCookTime( this.getCookTimeFromHtml(htmlPage) );
		this.setPreparation( this.getPreparationFromHtml(htmlPage) );
		this.setIngredients( this.getIngredientsFromHtml(htmlPage) );
		this.setDifficulty( this.getDifficultyFromHtml(htmlPage) );
		this.setSummary( this.getSummaryFromHtml(htmlPage) );
		this.setPrice( this.getPriceFromHtml(htmlPage) );
		this.setTitle( this.getTitleFromHtml(htmlPage) );
		this.setNote( this.getNoteFromHtml(htmlPage) );
		
		//in caso di errore nel recupero dell'url il valore è settato a null
		try {
			this.setImgUrl( this.getImgFromHtml(htmlPage) );
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			this.setImgUrl( null );
		}
		
		//in caso di errore nel recupero dell'url dell'immagine il valore è settato a null
		try {
			this.setUrl( new URI( htmlPage.baseUri() ) );
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			this.setUrl( null );
		}
		
		//setto recipehash
		try {
			this.setRecipeHash(Utils.getHashFromUrl(new URI(htmlPage.baseUri())));
		} catch (URISyntaxException e) {
			IRLogger.getSharedLogger().error("Impossibile calcolare hash per la ricetta " + htmlPage.baseUri());
		}
		
		
	}//end-analyzePage
	
	
	/**
	 * analizza il DOM della pagina per restituire la difficolta' della ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getDifficultyFromHtml(Document htmlPage) {
		
		//array di stringhe temporaneo per eseguire le varie operazioni
		String[] tmp;
		
		//elementi nella pagine all'interno del tag con class="difficolta"
		Elements el = htmlPage.getElementsByClass("difficolta");
		//estrazione del testo
		try {
			tmp = el.get(0).text().split(": ");
			if( tmp.length == 2 ) {
				return tmp[1];
			}
		} catch (Exception e) {
			return null;
		}
		
		return null;
		
	}//end-getDifficultyFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire la difficolta' della ricetta
	 * @param Document htmlPage
	 * @return Double
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getNumberOfPeopleFromHtml(Document htmlPage) {
		
		//elemento nella pagine all'interno del tag con class="dosixpers"
		//Element el = htmlPage.select(".dosixpers .yield").first();
		Element el = htmlPage.select(".dosixpers").first();
		//estrazione del testo
		try {
			return el.text();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getNumberOfPeopleFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire il prezzo della ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getPriceFromHtml(Document htmlPage) {
		
		//array di stringhe temporaneo per eseguire le varie operazioni
		String[] tmp;
		
		//elemento nella pagine all'interno del tag con class="costo"
		Element el = htmlPage.getElementsByClass("costo").first();
		//estrazione del testo
		try {
			tmp = el.text().split(": ");
			if( tmp.length == 2 ) {
				return tmp[1];
			}
		} catch (Exception e) {
			return null;
		}
		
		return null;
		
	}//end-getPriceFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire eventuali note sulla ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getNoteFromHtml(Document htmlPage) {
		
		//elemento nella pagine all'interno del tag con class="difficolta"
		//Elements el = htmlPage.getElementsByClass("nota");
		Element el = htmlPage.getElementsByClass("nota").first();
		//estrazione del testo
		try {
			return el.html();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getNoteFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire eventuali note sulla ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getTitleFromHtml(Document htmlPage) {
		
		//elemento nella pagine all'interno del tag con class="difficolta"
		//Elements el = htmlPage.getElementsByClass("costo");
		Element el = htmlPage.select(".hrecipe h2").first();
		//estrazione del testo
		try {
			return el.html();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getTitleFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire eventuali note sulla ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 * @throws URISyntaxException 
	 */
	public URI getImgFromHtml(Document htmlPage) throws URISyntaxException {
		
		//immagine all'interno del tag con class="hrecipe"
		Element el = htmlPage.select(".hrecipe img[src]").first();
		String urlTmp = el.absUrl("src").replace(" ", "%20");
		
		//estrazione del link all'immagine
		try {
			URI uri = new URI( urlTmp );
			return uri;
		} catch (Exception e) {
			return null;
		}
		
	}//end-getImgFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire una breve descrizione della ricetta
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getSummaryFromHtml(Document htmlPage) {
		
		//elemento nella pagine all'interno del tag con class="difficolta"
		Element el = htmlPage.getElementsByClass("summary").first();
		//estrazione del testo
		try {
			return el.text();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getSummaryFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire il blocco relativo alla preparazione della ricetta
	 * (per blocco si intende l'insieme di testo e codice html)
	 * @param Document htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getPreparationFromHtml(Document htmlPage) {
		
		//elementi nella pagine all'interno del tag con class="paragrafi"
		Element el = htmlPage.getElementsByClass("paragrafi").first();
		//estrazione del testo
		try {
			return el.html();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getPreparationFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire il tempo minimo e massimo di cottura
	 * nel caso in cui fosse presente un solo valore, minimo e massimo sono identici
	 * @param htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getCookTimeFromHtml(Document htmlPage) {
		
		//elemento nella pagine all'interno del tag con class="cottura"
		Element el = htmlPage.getElementsByClass("cottura").first();
		//estrazione del testo
		try {
			return el.text();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getCookTimeFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire il tempo minimo e massimo di preparazione
	 * nel caso in cui fosse presente un solo valore, minimo e massimo sono identici
	 * @param htmlPage
	 * @return String
	 * @return null nel caso in cui la ricetta non abbia informazioni a riguardo
	 */
	public String getPrepareTimeFromHtml(Document htmlPage) {
		
		//elementi nella pagine all'interno del tag con class="preparazione"
		Element el = htmlPage.getElementsByClass("preparazione").first();
		//estrazione del testo
		try {
			return el.text();
		} catch (Exception e) {
			return null;
		}
		
	}//end-getPrepareTimeFromHtml
	
	
	/**
	 * analizza il DOM della pagina per restituire la collezione di ingredienti con relative quantita'
	 * @param htmlPage
	 * @return ArrayList<Ingredient>
	 */
	public ArrayList<Ingredient> getIngredientsFromHtml(Document htmlPage ) {
		
		ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
		
		Elements ingredienti = htmlPage.getElementsByClass("ingredient");
		try {
			
			for (Element i : ingredienti) {
				String name = i.getElementsByClass("name").first().text();
				URI uri = new URI( i.getElementsByClass("name").first().absUrl("href") );
				String amount = i.getElementsByClass("amount").first().text();
				ingredients.add( new Ingredient(name, amount, uri) );
			}
			
			return ingredients;
			
		} catch (Exception e) {
			return null;
		}
		
	}//end-getIngrediendtsFromHtml
}