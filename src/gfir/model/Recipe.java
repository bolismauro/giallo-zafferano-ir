package gfir.model;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;


public abstract class Recipe{
	
	//id univoco
	private int recipeHash;
	//titolo della ricetta
	private String title;
	//url della pagina della ricetta
	private URI url;
	//url dell'immagine della ricetta
	private URI imgUrl;
	//lista di ingredienti per la ricetta
	private List<Ingredient> ingredients;
	//difficolta' della ricetta
	private String difficulty;
	//tempo di cottura
	private String cookTime;
	//tempo di preparazione
	private String prepareTime;
	//numero di persone in base al quantitativo di ingredienti
	private String numberOfPeople;
	//prezzo della ricetta
	private String price;
	//note relative alla ricetta
	private String note;
	//breve descrizione della ricetta
	private String summary;
	//preparazione della ricetta
	private String preparation;
	//categorie della ricetta
	private List<String> categories;
	//score della ricetta in seguito alla ricerca
	private Float score;
	
	/**
	 * campo per indicare se la ricetta è stata analizzata
	 */
	private boolean analyzed;
	
	/**
	 * costruttore di default
	 */
	public Recipe() {
		this.title = null;
		this.url = null;
		this.imgUrl = null;
		this.ingredients = new ArrayList<Ingredient>();
		this.difficulty = null;
		this.cookTime = null;
		this.prepareTime = null;
		this.numberOfPeople = null;
		this.price = null;
		this.note = null;
		this.summary = null;
		this.preparation = null;
		this.categories = new ArrayList<String>();
		this.analyzed = false;
		this.score = (float) 0;
	}


	/**
	 * costruttore della classe
	 * @param title 
	 * @param url
	 * @param imgUrl
	 * @param ingredients
	 * @param difficulty
	 * @param cookTime
	 * @param prepareTime
	 * @param numberOfPeople
	 * @param price
	 * @param note
	 * @param summary
	 * @param preparation
	 */
	public Recipe(String title, URI url, URI imgUrl,
			ArrayList<Ingredient> ingredients, String difficulty,
			String cookTime, String prepareTime, String numberOfPeople, 
			String price, String note, String summary, String preparation) {
		super();
		this.recipeHash = url.hashCode();
		this.title = title;
		this.url = url;
		this.imgUrl = imgUrl;
		this.ingredients = ingredients;
		this.difficulty = difficulty;
		this.cookTime = cookTime;
		this.prepareTime = prepareTime;
		this.numberOfPeople = numberOfPeople;
		this.setPrice(price);
		this.note = note;
		this.summary = summary;
		this.preparation = preparation;
		this.analyzed = false;
		this.recipeHash = -1;
		this.score = (float) 0;
	}//end-constructor
	
	
	/**
	 * costruttore che crea una istanza della ricetta data una pagina web
	 * @param htmlPage
	 * @throws URISyntaxException 
	 */
	public Recipe(Document htmlPage) {
		analyzePage(htmlPage);
	}
	
	
	/**
	 * metodo che crea una istanza della ricetta data una pagina web
	 * @param htmlPage
	 * @return 
	 * @throws URISyntaxException 
	 */
	public void setRecipe(Document htmlPage) {
		analyzePage(htmlPage);
	}
	
	
	/**
	 * analizzatore di una pagina web per tornare le infomrazioni della ricetta
	 * @param htmlPage
	 * @throws URISyntaxException 
	 */
	public abstract void analyzePage(Document htmlPage);
	

	/**
	 * @return titolo della ricetta
	 */
	public String getTitle() {
		return this.title;
	}


	/**
	 * @param title titolo della ricetta
	 */
	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * @return url della della pagina della ricetta
	 */
	public URI getUrl() {
		return url;
	}


	/**
	 * @param url url della della pagina della ricetta
	 */
	public void setUrl(URI url) {
		this.url = url;
	}


	/**
	 * @return url dell'immagine della ricetta
	 */
	public URI getImgUrl() {
		return imgUrl;
	}


	/**
	 * @param imgUrl url dell'immagine della ricetta
	 */
	public void setImgUrl(URI imgUrl) {
		this.imgUrl = imgUrl;
	}


	/**
	 * @return ingredienti della ricetta
	 */
	public List<Ingredient> getIngredients() {
		return ingredients;
	}


	/**
	 * @param ingredients ingredienti della ricetta
	 */
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}


	/**
	 * @return difficolta' della ricetta
	 */
	public String getDifficulty() {
		return difficulty;
	}


	/**
	 * @param difficulty difficolta' della ricetta
	 */
	public void setDifficulty(String difficulty) {
		try {
			this.difficulty = difficulty.trim().toLowerCase().replace(" ", "_");
		} catch (Exception e) {
			this.difficulty = null;
		}
	}


	/**
	 * @return tempo di cottura
	 */
	public String getCookTime() {
		return cookTime;
	}


	/**
	 * @param cookTme tempo di cottura
	 */
	public void setCookTime(String cookTme) {
		this.cookTime = cookTme;
	}


	/**
	 * @return tempo di preparazione
	 */
	public String getPrepareTime() {
		return prepareTime;
	}


	/**
	 * @param prepareTime tempo di preparazione
	 */
	public void setPrepareTime(String prepareTime) {
		this.prepareTime = prepareTime;
	}


	/**
	 * @return numero di persone in base alle dosi fornite
	 */
	public String getNumberOfPeople() {
		return numberOfPeople;
	}


	/**
	 * @param numberOfPeople numero di persone in base alle dosi fornite
	 */
	public void setNumberOfPeople(String numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}


	/**
	 * @return prezzo della ricetta
	 */
	public String getPrice() {
		return price;
	}


	/**
	 * @param price prezzo della ricetta
	 */
	public void setPrice(String price) {
		try {
			this.price = price.trim().toLowerCase().replace(" ", "_");
		} catch (Exception e) {
			this.price = null;
		}
		
	}


	/**
	 * @return note sulla ricetta
	 */
	public String getNote() {
		return note;
	}


	/**
	 * @param note note sulla ricetta
	 */
	public void setNote(String note) {
		this.note = note;
	}


	/**
	 * @return breve descrizione della ricetta
	 */
	public String getSummary() {
		return customEscape(this.summary);
	}


	/**
	 * @param summary breve descrizione della ricetta
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}


	/**
	 * @return descrizione della preparazione della ricetta
	 */
	public String getPreparation() {
		return customEscape(this.preparation);
	}


	/**
	 * @param preparation descrizione della preparazione della ricetta
	 */
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}

	
	
	public synchronized List<String> getCategories() {
		return categories;
	}


	public synchronized void setCategories(List<String> categories) {
		this.categories = categories;
	}


	/**
	 * @return hash della ricetta
	 */
	public int getRecipeHash() {
		return recipeHash;
	}

	/**
	 * @param recipeHash
	 */
	public void setRecipeHash(int recipeHash) {
		this.recipeHash = recipeHash;
	}

	/**
	 * @return True se la ricetta è stata analizzata, false altrimenti
	 */
	public boolean isAnalyzed() {
		return analyzed;
	}
	

	/**
	 * @param analyzed Boleano che indica se la ricetta è stata analizzata
	 */
	public void setAnalyzed(boolean analyzed) {
		this.analyzed = analyzed;
	}
	
	
	/**
	 * @return score dopo la ricerca
	 */
	public Float getScore() {
		return score;
	}


	/**
	 * @param score score dopo la ricerca
	 */
	public void setScore(Float score) {
		this.score = score;
	}


	/**
	 * sostituzione dei caratteri speciali che hanno un comportamento indesiderato
	 * @param string
	 * @return
	 */
	private String customEscape(String string){
		return string.replace("&#8220;", "\"").replace("&#8217;", "'");
	}
	
	
}
