package gfir.crawler;

import gfir.utils.IRLogger;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class Robots {
	
	/**
	 * percorsi che non possono essere esaminati dal crawl
	 */
	private ArrayList<String> paths;
	/**
	 * user_agent di riferimento nel file 'robots.txt'
	 */
	private String userAgent;
	/**
	 * indirizzo della root del sito nel quale risiede il 
	 * file 'robots.txt' (root/robots.txt) 
	 */
	private String root;
	
	
	/**
	 * costruttore di default in cui vienene posto 
	 */
	public Robots() {
		
	}


	/**
	 * costruttore della classe
	 * @param userAgent
	 * @param root
	 * @throws IOException 
	 */
	public Robots(String userAgent, String root) throws IOException {
		this.paths = new ArrayList<String>();
		this.userAgent = userAgent;
		this.root = root + "robots.txt";
		this.setPaths();
	}
	
	
	/**
	 * costruttore della classe supponendo user_agent = *
	 * @param root
	 * @throws IOException 
	 */
	public Robots(String root) throws IOException {
		this("*",root);
	}
	
	
	/**
	 * @return ArrayList<String> percorsi che l'algoritmo di crawl non puo' visitare
	 */
	public ArrayList<String> getPaths() {
		return paths;
	}


	/**
	 * analisi del file 'robots.txt' per determinare l'elenco di percorsi
	 * che l'algoritmo di crawl non puo' visitare
	 * @throws IOException 
	 */
	public void setPaths() throws IOException {
		//lettura del file 'roots.txt'
		Document d = Jsoup.connect(this.root).get();
		//testo del file
		String text = d.text();
		//ricerca delle condizioni in base ai diversi user_agent
		String[] tt = text.split("User-agent: ");
		
		for (String string : tt) {
			if( string.startsWith( this.userAgent ) ) {
				//viene selezionata solo la porzione di testo
				//di interesse per l'analisi
				text = string;
			}
		}//end-for
		
		int i = 0;
		int start = 0;
		int end = 0;
		
		//ciclo che permette di selezionare tutti i percorsi che
		//l'algoritmo di crawl non puo' visitare
		while( text.indexOf("Disallow: ", i ) > 0 ) {
			
			start = text.indexOf(" ", text.indexOf("Disallow: ", i ) );
			if ( text.indexOf(" ", start+1 ) > 0 ) {
				end = text.indexOf(" ", start+1 );
			}
			else {
				end = text.length();
			}
			
			//viene aggiunto il percorso alla lista
			//(al percorso vengono eliminati eventuali spazi)
			this.paths.add( text.substring(start, end).replace(" ", "") );
			
			i = end;
		}//end-while
		
	}//end-setPaths()


	/**
	 * @return String agente che visita la pagina
	 */
	public String getUserAgent() {
		return userAgent;
	}


	/**
	 * @param userAgent agente che visita la pagina
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	/**
	 * @return String indirizzo al quale trovare il file 'robots.txt'
	 */
	public String getRoot() {
		return root;
	}


	/**
	 * @param root indirizzo al quale trovare il file 'robots.txt'
	 */
	public void setRoot(String root) {
		this.root = root;
	}
	
	
}
