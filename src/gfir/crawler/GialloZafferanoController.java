package gfir.crawler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gfir.crawler.interfaces.Controller;
import gfir.utils.IRLogger;

public class GialloZafferanoController implements Controller {

	public final int N_CRAWLER = 20;
	
	public GialloZafferanoController(){
		
	}
	
	
	/**
	 * Avvia la fase di crawling
	 */
	public void startCrawling() {
		
		// creo url store
		GialloZafferanoGraph urls = new GialloZafferanoGraph();
		//inizializzo
		try {
			URI seed = new URI("http://ricette.giallozafferano.it/");
			urls.setRobots("http://www.giallozafferano.it/");
			urls.addUri(seed, seed);
		} catch (URISyntaxException e) {
			IRLogger.getSharedLogger().fatal("Seed iniziale non valido");
		} catch (IOException e) {
			IRLogger.getSharedLogger().fatal("Impossibile caricare robots.txt");	
		}
		
		//creo data store
		HibernateDataStore dataStore = new HibernateDataStore();
        
		
		ArrayList<Thread> webCrawlerThreads = new ArrayList<Thread>();
        
		for(int i=0; i < N_CRAWLER; i++){
			
			GialloZafferanoWebCrawler webCrawler = new GialloZafferanoWebCrawler(urls, dataStore);
	        Thread t = new Thread(webCrawler);
	        webCrawlerThreads.add(t);
	        t.start();
	        
	        try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				IRLogger.getSharedLogger().info("Thread sleep error");
			}
		}	
		
		
		// attendo la terminazione dei thread crawler
		for(Thread t : webCrawlerThreads){
			try {
				t.join();
			} catch (InterruptedException e) {
				IRLogger.getSharedLogger().info("Thread join error");
			}
		}
		
		ArrayList<Integer> ids = dataStore.getRecipesHash();
		HashMap<Integer, ArrayList<String>> categories = urls.getCategories(ids);
		// e aggiorno le ricette
		dataStore.updateRecipes(categories);		
	}

	
	
}
