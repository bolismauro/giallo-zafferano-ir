package gfir.crawler;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import gfir.crawler.interfaces.DataStore;
import gfir.crawler.interfaces.UrlStore;
import gfir.crawler.interfaces.WebCrawler;
import gfir.model.GialloZafferanoRecipe;
import gfir.model.Recipe;
import gfir.utils.IRLogger;

public class GialloZafferanoWebCrawler extends WebCrawler{

	private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" 
            + "|png|tiff?|mid|mp2|mp3|mp4"
            + "|wav|avi|mov|mpeg|ram|m4v|pdf" 
            + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");	
	
	
	/*
	 * There is a better way? Creation time O(N) and search in O(1)
	 */
	private static final Set<String> GZ_ALLOWED_DOMAIN_NAME = new HashSet<String>(Arrays.asList(
		     new String[] { "www.giallozafferano.it", "ricette.giallozafferano.it" }
		));
	
	public GialloZafferanoWebCrawler(UrlStore urlStore, DataStore dataStore) {
		super(urlStore, dataStore);
	}

	protected static boolean shouldVisitUrl(URI uri){
		IRLogger.getSharedLogger().debug("Controllo url: "+uri);
		String strUrl = uri.toString();
		String domain = uri.getHost();
		return !FILTERS.matcher(strUrl).matches() && GZ_ALLOWED_DOMAIN_NAME.contains(domain);
        
	}	
	
	
	@Override
	protected boolean isReceipe(Document doc) {
		Elements elems = doc.select(".hrecipe");
		if (elems.size() == 0){
			return false;
		} else {
			return true;
		}
	}

	@Override
	protected void visit(Document d, URI pageUri) {
		/*
		 * We assume that recipe page is a leaf of our nodes graph
		 */
		if (this.isReceipe(d)) {
			
			IRLogger.getSharedLogger().debug("Trovata ricetta");
			Recipe r = new GialloZafferanoRecipe(d);
			this.dataStore.add(r);
			
		} else {
			
			// prelevo link  "piu visitati" --> problema categorie
			Elements outerLinks = d.select("a");
			Elements mostVisitedLinks =  null;
			try{
				mostVisitedLinks = d.select(".block-ricette-piu-viste").first().getAllElements();
			} catch (Exception e){
				//System.out.println("ERRORE "+ pageUri);
				// se non c'� elemento non faccio nulla
			}
			
			
			for (Element link : outerLinks) {
				
				if (mostVisitedLinks != null && mostVisitedLinks.contains(link)) {
					continue;
				}
				
				String absHref = link.attr("abs:href");
				if (absHref.equals("")) continue; // fix per link con operazioni js
				
				URI uri = null;
				try {
					
					uri = new URI(absHref);
					
				} catch (URISyntaxException e) {
					IRLogger.getSharedLogger().error("GZ WebCrawler , visit URISyntaxException ("+link+", "+e+")");
					continue;
				}
				
				if (GialloZafferanoWebCrawler.shouldVisitUrl(uri)) {
					IRLogger.getSharedLogger().debug("Aggiungo url "+uri);
					this.urlStore.addUri(pageUri, uri);
				}
				
			}
			
		}
	}

}
