package gfir.crawler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;


import gfir.crawler.interfaces.UrlStore;
import gfir.utils.IRLogger;
import gfir.utils.Utils;

public abstract class WebGraph implements UrlStore {
	
	/**
	 * elenco di nodi univoci visitati che, grazie alle loro informazioni di 
	 * interconnessione (ogni nodo presenta la lista degli inlink e degli outlink), 
	 * permette di ricostruire il grafo della porzione di sito analizzata
	 */
	HashMap<Integer, Node> graph;
	/**
	 * frontiera contenente le pagine ancora da visitare
	 */
	ArrayList<Node> frontier;
	/**
	 * restrizioni presenti nel file 'robots.txt' del sito
	 */
	Robots robots;
	
	
	/**
	 * costruttore di default
	 */
	public WebGraph() {
		this.graph = new HashMap<Integer, Node>();
		this.frontier = new ArrayList<Node>();
		this.robots = new Robots();
	}
	
	
	/**
	 * definizione dei parametri necessari per la lettura del file 'robots.txt'
	 * @param userAgent user agent che visita il sito
	 * @param root indirizzo nel quale cercare il file 'robots.txt'
	 * @throws IOException 
	 */
	public void setRobots(String userAgent, String root) throws IOException {
		this.robots = new Robots(userAgent, root);
	}
	
	
	/**
	 * definizione dell'indirizzo nel quale cercare il file 'robots.txt'
	 * supponendo user_agent = *
	 * @param root indirizzo nel quale cercare il file 'robots.txt'
	 * @throws IOException 
	 */
	public void setRobots(String root) throws IOException {
		this.setRobots("*", root);
	}
	
	
	/**
	 * @return HashMap<Integer, Node> grafo delle pagine visitate
	 */
	public synchronized HashMap<Integer, Node> getGraph() {
		return this.graph;
	}
	
	
	/**
	 * @return ArrayList<Node> frontiera
	 */
	public synchronized ArrayList<Node> getFrontier() {
		return this.frontier;
	}
	
	
	/**
	 * restituisce il primo nodo della frontiera da visitare
	 * @return
	 */
	public synchronized Node getNextNode() {
		try {
			Node next = this.frontier.get(0);
			this.frontier.remove(next);
			return next;
		} catch(Exception e) {
			return null;
		}
	}
	
	
	/**
	 * restituisce il primo url della frontiera da visitare
	 * @return URI 
	 */
	public synchronized URI getNextUri() {
		try {
			return this.getNextNode().getUrl();
		} catch(Exception e) {
			return null;
		}
	}
	
	
	/**
	 * aggiunge un insieme di url al grafo (alink del nodo visitato)
	 * @param originUri url del nodo visitato
	 * @param destinationUris insieme di url contenuti nella pagina del nodo visitato
	 */
	public synchronized void addUris(URI originUri, ArrayList<URI> destinationUris) {
		
		for (URI uri : destinationUris) {
			addUri(originUri, uri);
		}
		
	}//end-addUris
	
	
	/**
	 * aggiunge un url al grafo (outlink del nodo visitato)
	 * @param originUri url del nodo visitato
	 * @param destinationUris url contenuto nella pagina del nodo visitato
	 */
	public synchronized void addUri(URI originUri, URI destinationUri) {
		//fix gzf merging bug
		if (destinationUri.toString().startsWith("http://ricette.giallozafferano.it/ricetta/")) {
			IRLogger.getSharedLogger().debug("modifica url ("+destinationUri+")");
			String str = destinationUri.toString();
			str = str.replace("/ricetta", "") + ".html";
			
			try {
				destinationUri = new URI(str);
			} catch (URISyntaxException e) {
				IRLogger.getSharedLogger().error("ERRORE > url modificato");
			}
			IRLogger.getSharedLogger().debug("nuovo url ("+destinationUri+")");
		}
		
		
		//hash code dell'eticetta all'url del nodo origine
		Integer originHash = Utils.getHashFromUrl(originUri);
		
		//hash code dell'eticetta all'url del nodo destinazione
		Integer uriHash = Utils.getHashFromUrl(destinationUri);
		
		//controllo che il percorso non sia tra quelli vietati dal file 'robots.txt'
		if( !this.robots.getPaths().contains( destinationUri.getPath() ) ) {
		
			//l'url e' gia' associato ad un nodo del grafo che quindi viene aggiornato
			if( this.graph.keySet().contains(uriHash)  || this.graph.containsKey(uriHash)) {
				
				IRLogger.getSharedLogger().debug("aggiornamento grafo uri: " + destinationUri + " hash: " + uriHash);
				
				//controllo che il non esista gia' l'arco tra i due nodi
				boolean flagOutLink = this.graph.get(originHash).getAllOutLinks().contains( this.graph.get(uriHash) );
				boolean flagInLink = this.graph.get(uriHash).getAllInLinks().contains( this.graph.get(uriHash) );

				if( !flagOutLink ) {
					//inserimento del nuovo nodo tra gli outlink di quello visitato 
					this.graph.get(originHash).addOutLink( this.graph.get(uriHash) );
				}
				if( !flagInLink ) {
					//inserimento del nodo visitato tra gli inlink del nuovo nodo
					this.graph.get(uriHash).addInLink( this.graph.get(originHash) );
				}
			}//end-if
			
			//l'url non e' associato ad alcun nodo del grafo che quindi viene creato
			else {
				
				IRLogger.getSharedLogger().debug("nuovo nodo grafo uri: " + destinationUri + " hash: " + uriHash);
				
				//nuovo nodo da aggiungere all'hash map
				Node n = new Node();
				this.graph.put(uriHash, n);
				//inserimento del nuovo nodo tra gli outlink di quello visitato 
				this.graph.get(originHash).addOutLink(n);
				
				//url dellapagina relativa al nodo
				n.setUrl(destinationUri);
				//generazione dell'hash dell'url
				n.setHash();
				//creazione automatica di un label del nodo
				n.setLabel( Utils.getLabelFromUrl( destinationUri ) );
				
				//inserimento del nodo visitato tra gli inlink del nuovo nodo
				n.addInLink( this.graph.get(originHash) );
				//inserimento del nodo nella frontiera frontiera
				this.frontier.add(n);
			}//end-else
		
		}//end-if
		
	}//end-addUri
	
	
	/**
	 * aggiunge un url, e relativa etichetta, al grafo (outlink del nodo visitato)
	 * @param nodeHash
	 * @param uri
	 * @param label
	 */
	public synchronized void addUri(URI originUri, URI destinationUri, String label) {
		this.addUri(originUri, destinationUri);
		this.graph.get( destinationUri.hashCode() ).setLabel(label);
	}//end-addUri (con label)
	
	
	/**
	 * @param recipes
	 * @return HashMap<Integer,String> che associa l'hash di ogni url di una ricetta
	 * a tutti i nodi in essa entranti, ovvero le categorie
	 */
	public HashMap<Integer,ArrayList<String>> getCategories(ArrayList<Integer> recipes) {
		HashMap<Integer,ArrayList<String>> categories = new HashMap<Integer,ArrayList<String>>();	
		for (Integer recipe : recipes) {
			try{
				categories.put(recipe, this.graph.get(recipe).getInLinksLabels());
			} catch (Exception e){
				e.printStackTrace();
				IRLogger.getSharedLogger().error("Impossibile trovare categorie per ricetta "+recipe);
			}
		}
		
		return categories;
	}//end-getRecipes

}
