package gfir.crawler;

import gfir.utils.Utils;

import java.net.URI;
import java.util.ArrayList;

/**
 * classe che definisce la struttura di un nodo del grafo
 * 
 * possibile miglioramento:
 * 	aggiungere la lista degli inlink e outlink in modo da poter esplorare il
 *  grafo richiedendo minore tempo di computazione (a scapito di memoria pero')
 */
public class Node {
	
	
	/**
	 * hash code dell'url per favorire la ricerca
	 */
	private Integer hash;
	/**
	 * url della pagina relativa al nodo
	 */
	private URI url;
	/**
	 * etichetta del nodo
	 */
	private String label;
	/**
	 * lista dei nodi con un arco entrante nel nodo considerato
	 */
	private ArrayList<Node> inLinks;
	/**
	 * lista dei nodi con un arco uscente dal nodo considerato
	 */
	private ArrayList<Node> outLinks;
	
	
	/**
	 * costruttore di default
	 */
	public Node() {
		this.hash = 0;
		this.url = null;
		this.inLinks = new ArrayList<Node>();
		this.outLinks = new ArrayList<Node>();
	}


	/**
	 * costruttore della classe
	 * @param url
	 */
	public Node(URI url, String label, ArrayList<Node> inLinks, ArrayList<Node> outLinks) {
		super();
		this.url = url;
		//hash code generato direttamente dall'url
		this.setHash();
		this.label = label;
		this.inLinks = inLinks;
		this.outLinks = outLinks;
	}
	
	
	/**
	 * @return hash dell'url
	 */
	public Integer getHash() {
		return hash;
	}
	
	
	/**
	 * hash generato direttamente dall'url
	 */
	public void setHash() {
		this.hash = Utils.getHashFromUrl(this.url);
	}


	/**
	 * @return url della pagina relativa al nodo
	 */
	public URI getUrl() {
		return url;
	}
	
	
	/**
	 * @param url url della pagina relativa al nodo
	 */
	public void setUrl(URI url) {
		this.url = url;
	}


	/**
	 * @return etichetta del nodo
	 */
	public String getLabel() {
		return label;
	}


	/**
	 * @param label etichetta del nodo
	 */
	public void setLabel(String label) {
		this.label = label;
	}


	/**
	 * @return tutti gli inlink al nodo
	 */
	public ArrayList<Node> getAllInLinks() {
		return inLinks;
	}


	/**
	 * @param inLinks crea un insieme di inlink al nodo 
	 * sovrascrivendo una eventuale precedente collezione
	 */
	public void setInLinks(ArrayList<Node> inLinks) {
		this.inLinks = inLinks;
	}
	
	
	/**
	 * @param inLink aggiunge un nodo alla lista dei link entranti
	 */
	public void addInLink(Node inLink) {
		this.inLinks.add(inLink);
	}


	/**
	 * @return tutti gli outlink al nodo
	 */
	public ArrayList<Node> getAllOutLinks() {
		return outLinks;
	}


	/**
	 * @param outLinks crea un insieme di outlink al nodo 
	 * sovrascrivendo una eventuale precedente collezione 
	 */
	public void setOutLinks(ArrayList<Node> outLinks) {
		this.outLinks = outLinks;
	}
	
	
	/**
	 * @param outLink aggiunge un nodo alla lista dei link uscenti
	 */
	public void addOutLink(Node outLink) {
		this.outLinks.add(outLink);
	}
	
	
	/**
	 * @return ArrayList di label corrispondenti ai nodi con un
	 * arco entrante in quello considerato 
	 */
	public ArrayList<String> getInLinksLabels() {
		return this.toLabelsArray(this.inLinks);
	}//end-getInLinksLabels
	
	
	/**
	 * @return ArrayList di label corrispondenti ai nodi destinazione
	 * degli archi uscenti da quello considerato 
	 */
	public ArrayList<String> getOutLinksLabels() {
		return this.toLabelsArray(this.outLinks);
	}//end-getOutLinksLabels
	
	
	/**
	 * @param nodes
	 * @return dato un ArrayList di nodi, ritorna un array di label corrsipondenti
	 */
	private ArrayList<String> toLabelsArray(ArrayList<Node> nodes) {
		ArrayList<String> labels = new ArrayList<String>();
		for (Node node : nodes) {
			String label = node.getLabel();
			if (!labels.contains(label)){
				labels.add( node.getLabel() );
			}
		}
		return labels;
	}//end-toLabelsArrays


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		return true;
	}
	
}
