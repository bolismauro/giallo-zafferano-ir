package gfir.crawler.interfaces;

import gfir.utils.IRLogger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public abstract class WebCrawler implements Runnable{
	
	/**
	 * store utilizzato per ottenere gli url
	 */
	protected UrlStore urlStore;
	
	/**
	 * store utilizzato per memorizzare le ricette
	 */
	protected DataStore dataStore;
	
	/**
	 * Costruttore
	 * @param urlStore store utilizzato per ottenere gli url
	 * @param dataStore store utilizzato per memorizzare le ricette
	 */
	public WebCrawler(UrlStore urlStore, DataStore dataStore) {
		super();
		this.urlStore = urlStore;
		this.dataStore = dataStore;
	}

	
	public void run(){
		
		URI nextUrl = this.urlStore.getNextUri();
		
		while (nextUrl != null) {
			
			IRLogger.getSharedLogger().debug("Visito "+nextUrl);
			
			Document d = null;
			
			try {
				d = Jsoup.connect(nextUrl.toURL().toString()).get();
				this.visit(d, nextUrl);
			} catch (MalformedURLException e) {
				IRLogger.getSharedLogger().error("WebCrawler , run MalformedURLException ("+nextUrl+", "+e+")");
			} catch (IOException e) {
				IRLogger.getSharedLogger().error("WebCrawler , run IOException ("+nextUrl+", "+e+")");
			}
			
			
			nextUrl = this.urlStore.getNextUri();
		}
		
	}
	
	/**
	 * Questo metodo permette di determinare se una certa risorsa remota debba essere visitata dal carwler
	 * @param uri L'uri della risorsa  
	 * @return True se il crawler deve visitare la risorsa, False altrimenti
	 */
	protected static boolean shouldVisitUrl(URI uri){
		return true;
	}
	
	/**
	 * Questo metodo permette di capire se una determinata pagina remota contiene una ricetta
	 * @param doc La pagina remota
	 * @return True se la pagina remota � una ricetta, False altrimenti
	 */
	protected abstract boolean isReceipe(Document doc);
	
	/**
	 * Questo metodo viene richiamato dal run una volta che la pagina � stata scaricata. Questo metodo viene generalemente utilizzato per effettuare
	 * delle analisi della pagina o altre operazioni
	 * @param d Il documento scaricato
	 */
	protected abstract void visit(Document d, URI pageUri);
	
	
}
