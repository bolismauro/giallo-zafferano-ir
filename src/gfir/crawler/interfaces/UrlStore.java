package gfir.crawler.interfaces;

import gfir.crawler.Node;

import java.net.URI;
import java.util.ArrayList;

public interface UrlStore {
	
	/**
	 * restituisce il primo url della frontiera da visitare
	 * @return URI 
	 */
	public abstract URI getNextUri();
	
	
	/**
	 * aggiunge un insieme di url al grafo (alink del nodo visitato)
	 * @param originUri url del nodo visitato
	 * @param destinationUris insieme di url contenuti nella pagina del nodo visitato
	 */
	public abstract void addUris(URI originUri, ArrayList<URI> destinationUris);
	
	
	/**
	 * aggiunge un url al grafo (outlink del nodo visitato)
	 * @param originUri url del nodo visitato
	 * @param destinationUris url contenuto nella pagina del nodo visitato
	 */
	public abstract void addUri(URI originUri, URI destinationUri);
	
}
