package gfir.crawler.interfaces;

import org.apache.lucene.index.IndexWriter;

public abstract class Indexer implements Runnable{

	protected DataStore dataStore;
	protected IndexWriter index;
	
	public Indexer(DataStore dataStore,IndexWriter index) {
		super();
		this.dataStore = dataStore;
		this.index=index;
	}

	public void run(){}
}
