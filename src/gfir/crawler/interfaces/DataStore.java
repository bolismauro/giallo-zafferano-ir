package gfir.crawler.interfaces;

import gfir.model.Recipe;

public interface DataStore {
	
	/**
	 * aggiunge una ricetta allo store
	 * @param r la ricetta da aggiungere
	 */
	public abstract void add(Recipe r);
	
	/**
	 * torna una ricetta da analizzare
	 * @return Recipe r La ricetta da analizzare
	 */
	public abstract Recipe getNextRecipe();
	
	
	//public abstract void setRecipeDone();	
}

