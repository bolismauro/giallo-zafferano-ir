package gfir.crawler.interfaces;

/**
 * Interfaccia per le classi controller
 * Un controller si occupa di avviare tutta la fase di crawling 
 */
public interface Controller {
	
	/**
	 * Avvia la fase di crawling
	 */
	public abstract void startCrawling();
	
}
