package gfir.crawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

import gfir.crawler.interfaces.DataStore;
import gfir.model.GialloZafferanoRecipe;
import gfir.model.Recipe;
import gfir.utils.IRLogger;

public class HibernateDataStore implements DataStore {
	
	/**
	 * Session factory per Hibernate, permette di creare le sessioni di lavoro con il db
	 */
	private SessionFactory sessionFactory;

	
	public HibernateDataStore() {
		super();
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
		sessionFactory = cfg.buildSessionFactory();
	}

	
	/**
	 * aggiunge una ricetta allo store
	 * @param r la ricetta da aggiungere
	 */
	public void add(Recipe r) {
		try{
			//creo sessione
			Session session = sessionFactory.openSession();
			// creo la transazione
			Transaction transaction = session.beginTransaction();
			//aggiungo la ricetta
			session.save(r);
			//committo la transazione e salvo la sessione
			transaction.commit();
			session.close();
		} catch (Exception e){
			IRLogger.getSharedLogger().error("Impossibile salvare ricetta " + r.getTitle() + "(" + r.getRecipeHash() + ")");
			e.printStackTrace();
		}
	}

	
	/**
	 * torna una ricetta da analizzare
	 * @return Recipe r La ricetta da analizzare
	 */
	public synchronized Recipe getNextRecipe() {
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from GialloZafferanoRecipe gzr where gzr.analyzed = 0");
		query.setMaxResults(1);
		List list = query.list();
		
		if ( list.size() > 0) {
			// fix problem with closing session
			Recipe r = (Recipe)list.get(0);
			Hibernate.initialize(r.getCategories());
			Hibernate.initialize(r.getIngredients());
			Transaction t = session.beginTransaction();
			r.setAnalyzed(true);
			session.save(r);
			t.commit();
			session.close();
			return r;
		} else {
			session.close();
			return null;
		}
	}
	
	/**
	 * Torna gli id di tutte le ricette salvate sullo store
	 * @return List<Integer> lista di id
	 */
	public ArrayList<Integer> getRecipesHash(){
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("select recipeHash from GialloZafferanoRecipe gzr");
		ArrayList<Integer> list = (ArrayList<Integer>) query.list();
		return list;	
	}
	
	public void updateRecipes(HashMap<Integer, ArrayList<String>> map){
		Session session = sessionFactory.openSession();
		
		Set<Integer> keys = map.keySet();
		
		for(Integer key : keys){
			Transaction transaction = session.beginTransaction();
			GialloZafferanoRecipe t = (GialloZafferanoRecipe) session.get(GialloZafferanoRecipe.class, key);
			t.setCategories(map.get(key));
			session.save(t);
			transaction.commit();	
		}
		
		session.clear();
	}

}
