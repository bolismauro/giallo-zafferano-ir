package gfir;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

public class HibernateSchemaGenerator {

	/**
	 * Questa classe permette di generare lo schema necessario a hibernate per inserire i dati delle ricette
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
		cfg.setProperty("hibernate.hbm2ddl.auto","create");
		SchemaUpdate export = new SchemaUpdate(cfg);
	    export.execute(true, true);
	}

}
