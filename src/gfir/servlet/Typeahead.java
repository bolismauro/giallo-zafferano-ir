package gfir.servlet;

import gfir.model.GialloZafferanoRecipe;
import gfir.query.SearchEngine;
import gfir.query.model.QuerySearch;
import gfir.utils.Constant;
import gfir.utils.Index;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.FilterAtomicReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.spell.DirectSpellChecker;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class Typeahead
 */
public class Typeahead extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Typeahead() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String folder = "gialloZafferanoIndex";
		String path = getServletContext().getRealPath("");
		if (path.endsWith("/")) {
			path = path + folder;
		} else {
			path = path + "/" + folder;
		}

		File indexFolder = new File(path);
		Directory directory = FSDirectory.open(indexFolder);

		DirectoryReader reader = DirectoryReader.open(directory);
		IndexSearcher index = new IndexSearcher(reader);

		// la risposta e' di tipo json
		response.setContentType("application/json");
		response.setCharacterEncoding("ISO-8859-15");
		// out permette di scrivere a video
		PrintWriter out = response.getWriter();

		String query = request.getParameter("query");

		// classe per la trasformazione di file json in classi e viceversa
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();

		
		SpellChecker spell= new SpellChecker(directory);
		spell.indexDictionary(new LuceneDictionary(reader,Index.TITLE), 
				new IndexWriterConfig(Version.LUCENE_40, Index.INDEX_ANALYZER.get(Index.TITLE)), false);
		
		spell.setAccuracy( (float) 0.1);
		//System.out.println( spell.getAccuracy() + " | " + spell.getStringDistance() );
		
		String[] l = spell.suggestSimilar(query, 100);
		
		System.out.println("numero risultati: " + l.length);
		for (String string : l) {
			System.out.print(string + ", ");
		}
		
		out.print(gson.toJson(l));

	}

}
