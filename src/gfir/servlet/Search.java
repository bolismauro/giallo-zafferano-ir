package gfir.servlet;

import gfir.model.GialloZafferanoRecipe;
import gfir.query.SearchEngine;
import gfir.query.model.QuerySearch;
import gfir.utils.Constant;
import gfir.utils.Index;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.queryparser.classic.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class Search
 */
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//la risposta e' di tipo json
		response.setContentType("application/json");
		response.setCharacterEncoding("ISO-8859-15");
		//out permette di scrivere a video
		PrintWriter out = response.getWriter();
		
		//classe per la trasformazione di file json in classi e viceversa
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		
		//lettura del json inviato in post
		QuerySearch qs = gson.fromJson(request.getParameter("json"), QuerySearch.class);
		
		int max = Constant.MAX_SEARCH_RESULTS;
		if( qs.getMaxResults() > 0 ) {
			max = qs.getMaxResults();
		}
		
		//creazione dell'istanza del motore di ricerca
		SearchEngine se = new SearchEngine(qs, max, getServletContext().getRealPath(""));
		
		
		// lista contenente i risultati della query
		ArrayList<GialloZafferanoRecipe> gzr = new ArrayList<GialloZafferanoRecipe>();
		try {
			//popolamento della lista di risultati
			gzr = se.output( se.avviaRicerca() );
			//generazione dell'output formato json
			out.print( gson.toJson(gzr) );
			
		} catch (IOException e) {
			out.print("{");
			out.print("\"message\": \"error\",");
			out.print("\"exception\": \"IOException\",");
			out.print("\"stackTrace\": \"" + e.getMessage() + "\"" );
			out.print("}");
			
		} catch (URISyntaxException e) {
			out.print("{");
			out.print("\"message\": \"error\",");
			out.print("\"exception\": \"URISyntaxException\",");
			out.print("\"stackTrace\": \"" + e.getMessage() + "\"" );
			out.print("}");
			
		} catch (ParseException e) {
			out.print("{");
			out.print("\"message\": \"error\",");
			out.print("\"exception\": \"ParseException\",");
			out.print("\"stackTrace\": \"" + e.getMessage() + "\"" );
			out.print("}");
		}
		
	}//end-doPost

}
