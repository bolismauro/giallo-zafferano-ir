package gfir;

import gfir.crawler.GialloZafferanoController;
import gfir.crawler.HibernateDataStore;

public class RunCrawling {

	/**
	 * Main, avvia il controller
	 * @param args
	 */
	public static void main(String[] args) {
		new GialloZafferanoController().startCrawling();
	}
}
