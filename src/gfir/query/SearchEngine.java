package gfir.query;

import gfir.analyzer.StandardAnalizzatoreItaliano;
import gfir.model.GialloZafferanoRecipe;
import gfir.model.Ingredient;
import gfir.query.model.QueryItem;
import gfir.query.model.QuerySearch;
import gfir.utils.IRLogger;
import gfir.utils.Index;
import gfir.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.CachingWrapperFilter;
import org.apache.lucene.search.FieldComparator.NumericComparator;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.join.ScoreMode;
import org.apache.lucene.search.join.ToParentBlockJoinQuery;
import org.apache.lucene.search.similarities.BasicStats;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.SimilarityBase;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.hibernate.mapping.Collection;

public class SearchEngine {

	// Percorso cartella indice
	private final static String FOLDER = "gialloZafferanoIndex";
	private String path;
	private File indexFolder;
	private Directory directory;

	// Apertura indice
	private DirectoryReader reader;
	private IndexSearcher index;

	// classe contenente gli elementi della query
	private QuerySearch querySearch;

	// numero massimo di documenti reperiti
	private int max;

	/**
	 * Costruttore
	 * 
	 * @param fields
	 *            lista campi
	 * @param max
	 *            numero massimo di documenti reperiti
	 */
	public SearchEngine(QuerySearch querySearch, int max, String path) {
		this.querySearch = querySearch;
		this.max = max;
		if (path.endsWith("/")) {
			this.path = path + FOLDER;
		} else {
			this.path = path + "/" + FOLDER;
		}
	}

	/**
	 * data una stringa vengono eliminate le stopwords
	 * 
	 * @param queryStr
	 * @param index
	 * @return
	 * @throws ParseException
	 */
	private String[] getSearchWords(String queryStr, String index)
			throws ParseException {
		// processo la stringa fuzzy
		Query parsedQuery = new QueryParser(Version.LUCENE_40, index,
				Index.INDEX_ANALYZER.get(index)).parse(queryStr);
		// ritorno l'array di parole da cercare
		String str = parsedQuery.toString();
		
		
		return str.toString().split(index + ":");
	}// end-getSearchWords

	/**
	 * Metodo che effettua la ricerca e ritorna un array di documenti
	 * 
	 * @return ScoreDoc[] array di documenti
	 * @throws IOException
	 * @throws ParseException
	 */
	public ScoreDoc[] avviaRicerca() throws IOException, ParseException {

		// Permette di agganciare la pagina master del segmento partendo dalle
		// figlie nested
		Filter pagina = new CachingWrapperFilter(new QueryWrapperFilter(
				new TermQuery(new Term(Index.DOC_TYPE, Index.DOC_RECIPE))));
		// query finale con la quale effettuare la ricerca
		BooleanQuery query = new BooleanQuery();
		// quey con i blocchi in OR tra loro
		BooleanQuery subQueryOr = new BooleanQuery();
		// quey con i blocchi in AND tra loro
		BooleanQuery subQueryAnd = new BooleanQuery();
		// termine generico di una query
		Query queryTerm = null;
		
		boolean suggestions = true;

		
		// parte della query relativa alla stringa proveniente dal form di
		// ricerca
		String queryStr = this.querySearch.getSearchInput();
		if (queryStr != null && !queryStr.equals("")) {
			// la sottoquery viene messa in AND alle altre
			subQueryAnd.add(this.makeQuerySearchInput(queryStr, pagina), Occur.MUST);
			subQueryOr.add(this.makeQuerySearchInput(queryStr, pagina), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if searchInput
		

		// parte della query relativa agli ingredienti
		ArrayList<QueryItem> ingredients = this.querySearch.getIngredients();
		if (ingredients.size() > 0) {
			for (QueryItem queryItem : ingredients) {
				// aggiungo la query nested alla query principale
				subQueryAnd.add(this.makeQueryIngredients(queryItem, pagina), Occur.MUST);
				subQueryOr.add(this.makeQueryIngredients(queryItem, pagina), Occur.SHOULD);
			}// end-for
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if ingredients
		

		// parte della query relativa alla difficolta'
		if (this.querySearch.getDifficulty() != null
				&& !this.querySearch.getDifficulty().equals("")) {
			
			String difficulty = this.querySearch.getDifficulty();
			
			subQueryAnd.add(this.makeQueryDifficulty(difficulty), Occur.MUST);
			subQueryOr.add(this.makeQueryDifficulty(difficulty), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if difficulty
		

		// parte della query relativa al prezzo
		String price = this.querySearch.getPrice();
		// valori limite per i quali saltare la ricerca
		String priceBound = "0-" + (Index.PRICE_VALUES.length - 1);
		if (price != null && !price.equals("") && !price.equals(priceBound)) {
			subQueryAnd.add(this.makeQueryPrice(price), Occur.MUST);
			subQueryOr.add(this.makeQueryPrice(price), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if price
		

		// parte della query relativa al tempo di preparazione
		String prepareTime = this.querySearch.getPrepareTime();
		// valori limite per i quali saltare la ricerca
		String prepareTimeBound = Index.MIN_PREPARE_TIME.intValue() + "-"
				+ Index.MAX_PREPARE_TIME.intValue();
		if (prepareTime != null && !prepareTime.equals("")
				&& !prepareTime.equals(prepareTimeBound)) {
			
			subQueryAnd.add(this.makeQueryPrepareTime(prepareTime), Occur.MUST);
			subQueryOr.add(this.makeQueryPrepareTime(prepareTime), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if preparazione
		

		// parte della query relativa al tempo di cottura
		String cookTime = this.querySearch.getCookTime();
		// valori limite per i quali saltare la ricerca
		String cookTimeBound = Index.MIN_COOK_TIME.intValue() + "-"
				+ Index.MAX_COOK_TIME.intValue();
		if (cookTime != null && !cookTime.equals("")
				&& !cookTime.equals(cookTimeBound)) {
			
			subQueryAnd.add(this.makeQueryCookTime(cookTime), Occur.MUST);
			subQueryOr.add(this.makeQueryCookTime(cookTime), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if cottura
		

		// parte della query relativa al numero di persone
		String numberOfPeople = this.querySearch.getNumberOfPeople();
		// valori limite per i quali saltare la ricerca
		String numberOfPeopleBound = Index.MIN_NUMBER_OF_PEOPLE.intValue()
				+ "-" + Index.MAX_NUMBER_OF_PEOPLE.intValue();
		if (numberOfPeople != null && !numberOfPeople.equals("")
				&& !numberOfPeople.equals(numberOfPeopleBound)) {

			subQueryAnd.add(this.makeQueryNumberOfPeople(numberOfPeople), Occur.MUST);
			subQueryOr.add(this.makeQueryNumberOfPeople(numberOfPeople), Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if numero di persone
		

		// parte della query relativa alla ricerca delle ricette
		// appartenenti ad una specifica categoria
		String category = this.querySearch.getCategory();
		if (category != null && !category.equals("")) {

			queryTerm = new FuzzyQuery(new Term(Index.CATEGORY, category),
					Index.QUERY_FEATURES.get(Index.CATEGORY));
			queryTerm.setBoost(0);
			subQueryAnd.add(queryTerm, Occur.MUST);
			subQueryOr.add(queryTerm, Occur.SHOULD);
			// non viene fatta la ricerca per suggerimenti
			suggestions = false;
		}// end-if searchInput
		
		
		// ricerca le ricette con like
		ArrayList<String> likes = this.querySearch.getLikes();
		if (suggestions && likes != null && likes.size() > 0) {
			// sottoquery per la ricerca di una ricetta
			BooleanQuery subSubQuery = null;
			
			for (String like : likes) {
				
				subSubQuery = new BooleanQuery();
				// ricerca in base all'hash della ricetta
				queryTerm = new FuzzyQuery(new Term(Index.HASH, like),0);
				queryTerm.setBoost(0);
				subSubQuery.add(queryTerm, Occur.MUST);
				// ricerca di una ricetta e non ingrediente
				queryTerm = new FuzzyQuery(new Term(Index.DOC_TYPE, Index.DOC_RECIPE),0);
				queryTerm.setBoost(0);
				subSubQuery.add(queryTerm, Occur.MUST);
				
				subQueryAnd.add(subSubQuery, Occur.SHOULD);
				subQueryOr.add(subSubQuery, Occur.SHOULD);
			}
		}// end-if searchInput
		
		query.add(subQueryAnd, Occur.SHOULD);
		query.add(subQueryOr, Occur.SHOULD);
		
		

		// Creazione della collezione contente al piu' max documenti
		TopScoreDocCollector collector = TopScoreDocCollector.create(max, true);
		// apertura indice
		aperturaIndice();
		// avvio ricerca
		index.search(query, collector);
		// immagazzino i risultati
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		// chiusura indice
		chiusuraIndice();
		// restituisco i risultati
		return hits;

	}// end-avviaRicerca

	/**
	 * Metodo di apertura indice in modalita' lettura
	 * 
	 * @throws IOException
	 */
	private void aperturaIndice() throws IOException {

		// Reperimento indice
		indexFolder = new File(path);
		directory = FSDirectory.open(indexFolder);

		// Apertura indice
		reader = DirectoryReader.open(directory);
		index = new IndexSearcher(reader);

		// Similarita' di default
		Similarity sim = new DefaultSimilarity();

		index.setSimilarity(sim);

	}// end-aperturaIndice

	/**
	 * Metodo di chiusura indice in lettura
	 * 
	 * @throws IOException
	 */
	private void chiusuraIndice() throws IOException {
		directory.close();
		reader.close();
	}// end-chiusuraIndice

	/**
	 * Metodo che effettua un outoput a console dato un array di documenti
	 * 
	 * @param hits
	 * @return ArrayList<Recipe>
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public ArrayList<GialloZafferanoRecipe> output(ScoreDoc[] hits)
			throws IOException, URISyntaxException {
		// lista di ricette ottenute dalla ricerca
		ArrayList<GialloZafferanoRecipe> res = new ArrayList<GialloZafferanoRecipe>();

		// apertura dell'indice per la ricerca
		aperturaIndice();

		IRLogger.getSharedLogger().debug("Trovati " + hits.length + " hits.");
		
		// creazione della lista delle ricette
		for (int i = 0; i < hits.length; i++) {
			res.add( this.recipeFromDocument(hits[i]) );
		}

		// chiusura dell'indice
		chiusuraIndice();
		
		Collections.sort( res, new Comparator<GialloZafferanoRecipe>() {

			public int compare(GialloZafferanoRecipe o1,
					GialloZafferanoRecipe o2) {
				return o2.getScore().compareTo(o1.getScore());
			}
		});
		
		System.out.println("\nPost like:");
		for (GialloZafferanoRecipe gialloZafferanoRecipe : res) {
			System.out.println( gialloZafferanoRecipe.getScore() + " - " + gialloZafferanoRecipe.getRecipeHash() );
		}
		
		return res;
	}// end-output

	/**
	 * Metodo che, dato l'identificativo di un documento, ricerca tutti i
	 * documenti contenenti gli ingredienti ad esso affini
	 * 
	 * @param docID
	 * @return ArrayList<Integer>
	 * @throws IOException
	 */
	protected ArrayList<Integer> getNestedDocs(int docID) throws IOException {

		ArrayList<Integer> docIDs = new ArrayList<Integer>();

		int i = docID - 1;

		for (; i >= 0; i--) {
			Document d = index.doc(i);
			// se il documento non contiene il campo ingrediente,
			// la lista degli ingredienti per la ricetta scelta e' ultimata
			if (d.get(Index.DOC_TYPE) != null
					&& d.get(Index.DOC_TYPE).equals(Index.DOC_RECIPE)) {
				break;
			}
			// altrimenti aggiungo l'ingrediente alla lista
			else {
				docIDs.add(i);
			}
		}// end-for

		return docIDs;
	}// end-getNestedDocs

	/**
	 * Metodo che, presa il documento risultato dalla ricerca, ne restituisce
	 * una ricetta
	 * 
	 * @param docID
	 * @param first indica se la ricetta e' la prima oppure no
	 * @return GialloZafferanoRecipe
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	protected GialloZafferanoRecipe recipeFromDocument(ScoreDoc hit)
			throws IOException, URISyntaxException {
		// ricetta
		GialloZafferanoRecipe recipe = new GialloZafferanoRecipe();

		// documento contenente i dati della ricetta
		Document d = index.doc(hit.doc);
		// indice dei documenti che contengono gli ingredienti della ricetta
		ArrayList<Integer> docIDs = this.getNestedDocs(hit.doc);

		// inserimento dei dati nella classe
		recipe.setRecipeHash(Integer.valueOf(d.get(Index.HASH)));
		recipe.setTitle(d.get(Index.TITLE));
		try {
			recipe.setUrl(new URI(d.get(Index.URL)));
		} catch (Exception e) {
			System.out.println("url error: " + d.get(Index.URL));
		}
		// se l'immagine non e' presente
		try {
			recipe.setImgUrl(new URI(d.get(Index.IMG_URL)));
		} catch (Exception e) {
			recipe.setImgUrl(new URI("http://www.placeholder.it/210x150"));
		}
		recipe.setDifficulty(d.get(Index.DIFFICULTY));
		recipe.setCookTime(d.get(Index.COOK_TIME));
		recipe.setPrepareTime(d.get(Index.PREPARE_TIME));
		recipe.setNumberOfPeople(d.get(Index.NUMBER_OF_PEOPLE));
		recipe.setPrice(d.get(Index.PRICE));
		recipe.setNote(d.get(Index.NOTE));
		recipe.setSummary(d.get(Index.SUMMARY));
		recipe.setPreparation(d.get(Index.PREPARATION));
		// lista delle categorie legate alla ricetta
		ArrayList<String> categories = new ArrayList<String>();
		// creazione della lista
		for (int i = 0; i < d.getFields(Index.CATEGORY).length; i++) {
			categories.add(d.getFields(Index.CATEGORY)[i].stringValue());
		}
		recipe.setCategories(categories);

		// lista degli ingredienti legati alla ricetta
		ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
		// creazione della lista

		for (int id : docIDs) {
			try {
				Document di = index.doc(id);
				String name = di.get(Index.INGREDIENT);
				String amount = di.get(Index.AMOUNT);
				URI url = new URI(di.get(Index.INGREDIENT_URL));
				ingredients.add(new Ingredient(name, amount, url));
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}// end-for
		recipe.setIngredients(ingredients);

		// score derivato dalla ricerca pesato in base ai like
		if( this.querySearch.getLikes() != null && 
			this.querySearch.getLikes().size() > 0 && 
			this.querySearch.getLikes().contains( String.valueOf(recipe.getRecipeHash()) ) ) {
			recipe.setScore(hit.score * Index.LIKE_BOOST);
		} else {
			recipe.setScore(hit.score);
		}

		IRLogger.getSharedLogger().debug(
				hit.score + ". " + hit.doc + " - " + d.get(Index.HASH) + " - "
						+ d.get(Index.URL));

		if( this.querySearch.getLikes() != null && 
			this.querySearch.getLikes().size() > 0 && 
			this.querySearch.getLikes().contains( String.valueOf(recipe.getRecipeHash()) ) ) {
			System.out.println(" - boost!!");
		} else {
			System.out.println(" ");
		}
		return recipe;
	}// end-RecipeFromDocument

	/**
	 * metodo che permette di ridurre il boos al crescere della distanza di
	 * Levenshtein
	 * 
	 * @param boost
	 *            boost associato all'indice di default
	 * @param length
	 *            distanza di Levenshtein attuale
	 * @param maxLength
	 *            distanza di Levenshtein di default per l'indice
	 * @return
	 */
	private float getWeightedBoost(float boost, int length, int maxLength) {
		float x = (float) (Math.log(length + 1) / Math.log(maxLength + 1.3));
		return boost - boost * x;
	}// end-getWeightedBoost

	/**
	 * metodo che crea la parte di query relativa alla ricerca principale
	 * 
	 * @param queryString
	 * @return
	 * @throws ParseException
	 */
	private BooleanQuery makeQuerySearchInput(String queryStr, Filter pagina)
			throws ParseException {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;

		// converto la stringa in un array di parole
		// n.b. l'indice che si passa non e' utilizzato, pertanto
		// puo' essere generico
		String[] subQueryStr = this.getSearchWords(queryStr, Index.TITLE);
		

		
		// indici sui quali effettuare la ricerca del testo proveniente dal
		// form di ricerca
		for (String index : Index.SEARCH_INPUT) {
			// distanza di Levenshtein di default per l'indice
			int length = Index.QUERY_FEATURES.get(index);

			// valutazione di tutti i termini sui diversi campi e per
			// diverse distanze di edit, tutte in OR tra loro
			for (int i = 0; i <= length; i++) {
				for (String str : subQueryStr) {
					if (!str.trim().equals("")) {

						if (index.equals(Index.INGREDIENT)) {
							subQuery.add(
									this.makeQueryIngredient(str, pagina, i),
									Occur.SHOULD);
						} else {
							queryTerm = new FuzzyQuery(new Term(index, str), i);
							// specifica del boost
							queryTerm.setBoost(this.getWeightedBoost(
									Index.QUERY_BOOST.get(index), i, length));
							// aggiungo la query parser al modello boolenao
							subQuery.add(queryTerm, Occur.SHOULD);
						}

					}// end-if
				}// end-for parole query
			}// end-for distanze di edit

		}// end-ciclo suigli indici
		
		return subQuery;
	}// end-makeQuerySearchInput

	/**
	 * metodo che crea la parte di query relativa alla ricerca per ingredienti
	 * 
	 * @param queryItem
	 * @param pagina
	 * @return
	 */
	private ToParentBlockJoinQuery makeQueryIngredients(QueryItem queryItem,
			Filter pagina) {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;
		// sottoquery sul nome dell'ingrediente
		BooleanQuery subSubQuery = new BooleanQuery();
		// distanza di Levenshtein di default per l'indice
		int length = Index.QUERY_FEATURES.get(Index.INGREDIENT);

		for (int i = 0; i <= length; i++) {
			// definizione dell'elemento INGREDIENT della query e del suo
			// boost
			queryTerm = new FuzzyQuery(new Term(Index.INGREDIENT,
					queryItem.getName()), i);
			queryTerm.setBoost(this.getWeightedBoost(
					Index.QUERY_BOOST.get(Index.INGREDIENT), i, length));
			subSubQuery.add(queryTerm, Occur.SHOULD);
		}// end-for
		subQuery.add(subSubQuery, Occur.MUST);

		if (!queryItem.getValue().equals("")) {

			// sottoquery sulla quantita' dell'ingrediente
			subSubQuery = new BooleanQuery();
			// distanza di Levenshtein di default per l'indice
			length = Index.QUERY_FEATURES.get(Index.INGREDIENT);

			for (int i = 0; i <= length; i++) {
				// definizione dell'elemento AMOUNT della query e del suo
				// boost
				queryTerm = new FuzzyQuery(new Term(Index.AMOUNT,
						queryItem.getValue()), i);
				queryTerm.setBoost(this.getWeightedBoost(
						Index.QUERY_BOOST.get(Index.AMOUNT), i, length));
				subSubQuery.add(queryTerm, Occur.SHOULD);
			}// end-for
			subQuery.add(queryTerm, Occur.MUST);
		}// end-if

		// aggiungo il collegamento al documento pagina
		ToParentBlockJoinQuery ingredientJoinQuery = new ToParentBlockJoinQuery(
				subQuery, pagina, ScoreMode.None);

		return ingredientJoinQuery;
	}// end-makeQueryIngredients

	/**
	 * metodo che permette di creare una query sul nome di un ingrediente
	 * 
	 * @param ingredient
	 * @param pagina
	 * @param i
	 *            distanza di edit compresa tra 0 e quella di default
	 * @return
	 */
	private ToParentBlockJoinQuery makeQueryIngredient(String ingredient,
			Filter pagina, int i) {
		Query queryTerm = null;
		// distanza di Levenshtein di default per l'indice
		int length = Index.QUERY_FEATURES.get(Index.INGREDIENT);

		// definizione dell'elemento INGREDIENT della query e del suo
		// boost
		queryTerm = new FuzzyQuery(new Term(Index.INGREDIENT, ingredient), i);
		queryTerm.setBoost(this.getWeightedBoost(
				Index.QUERY_BOOST.get(Index.INGREDIENT), i, length));

		// aggiungo il collegamento al documento pagina
		return new ToParentBlockJoinQuery(queryTerm, pagina, ScoreMode.None);
	}// end-makeQueryIngredient

	/**
	 * metodo che crea una query sull'indice difficulty
	 * 
	 * @param difficulties
	 * @return
	 */
	private BooleanQuery makeQueryDifficulty(String difficulties) {
		/*BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;
		for (String d : difficulties) {
			queryTerm = new FuzzyQuery(new Term(Index.DIFFICULTY, d),
					Index.QUERY_FEATURES.get(Index.DIFFICULTY));
			// specifica del boost
			queryTerm.setBoost(Index.QUERY_BOOST.get(Index.DIFFICULTY));
			// aggiungo la query parser al modello boolenao
			subQuery.add(queryTerm, Occur.SHOULD);
		}
		return subQuery;*/
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;
		// limite inferiore del range
		int min = Utils.getTimesFromArray(Utils.getNumbersFromString(difficulties))
				.get("min").intValue();
		// limite superiore del range
		int max = Utils.getTimesFromArray(Utils.getNumbersFromString(difficulties))
				.get("max").intValue();
		for (int i = min; i <= max; i++) {
			queryTerm = new FuzzyQuery(new Term(Index.DIFFICULTY,
					Index.DIFFICULTY_VALUES[i]),
					Index.QUERY_FEATURES.get(Index.DIFFICULTY));
			// specifica del boost
			queryTerm.setBoost(Index.QUERY_BOOST.get(Index.DIFFICULTY));
			// aggiungo la query parser al modello boolenao
			subQuery.add(queryTerm, Occur.SHOULD);
		}
		return subQuery;		
	}// end-makeQueryDifficulty

	/**
	 * metodo che crea una query sull'indice price
	 * 
	 * @param price
	 * @return
	 */
	private BooleanQuery makeQueryPrice(String price) {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;
		// limite inferiore del range
		int min = Utils.getTimesFromArray(Utils.getNumbersFromString(price))
				.get("min").intValue();
		// limite superiore del range
		int max = Utils.getTimesFromArray(Utils.getNumbersFromString(price))
				.get("max").intValue();
		for (int i = min; i <= max; i++) {
			queryTerm = new FuzzyQuery(new Term(Index.PRICE,
					Index.PRICE_VALUES[i]),
					Index.QUERY_FEATURES.get(Index.PRICE));
			// specifica del boost
			queryTerm.setBoost(Index.QUERY_BOOST.get(Index.PRICE));
			// aggiungo la query parser al modello boolenao
			subQuery.add(queryTerm, Occur.SHOULD);
		}
		return subQuery;
	}// end-makeQueryPrice

	/**
	 * metodo che crea una query sull'indice cookTime
	 * 
	 * @param cookTime
	 * @return
	 */
	private BooleanQuery makeQueryCookTime(String cookTime) {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;

		// limite inferiore del range
		Double min = Utils.getTimesFromArray(
				Utils.getNumbersFromString(cookTime)).get("min");
		// limite superiore del range
		Double max = Utils.getTimesFromArray(
				Utils.getNumbersFromString(cookTime)).get("max");

		// reale limite superiore del range
		max = Index.getUpperBound(Index.COOK_TIME, max);
		// query sul valore minimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.min(Index.COOK_TIME), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.COOK_TIME));
		subQuery.add(queryTerm, Occur.SHOULD);
		// query sul valore massimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.max(Index.COOK_TIME), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.COOK_TIME));
		subQuery.add(queryTerm, Occur.SHOULD);

		return subQuery;
	}// end-makeQueryCookTime

	/**
	 * metodo che crea una query sull'indice prepareTime
	 * 
	 * @param prepareTime
	 * @return
	 */
	private BooleanQuery makeQueryPrepareTime(String prepareTime) {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;
		// limite inferiore del range
		Double min = Utils.getTimesFromArray(
				Utils.getNumbersFromString(prepareTime)).get("min");
		// limite superiore del range
		Double max = Utils.getTimesFromArray(
				Utils.getNumbersFromString(prepareTime)).get("max");

		// reale limite superiore del range
		max = Index.getUpperBound(Index.PREPARE_TIME, max);
		// query sul valore minimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.min(Index.PREPARE_TIME), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.PREPARE_TIME));
		subQuery.add(queryTerm, Occur.SHOULD);
		// query sul valore massimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.max(Index.PREPARE_TIME), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.PREPARE_TIME));
		subQuery.add(queryTerm, Occur.SHOULD);

		return subQuery;
	}// end-makeQueryPrepareTime

	/**
	 * metodo che crea una query sull'indice price
	 * 
	 * @param price
	 * @return
	 */
	private BooleanQuery makeQueryNumberOfPeople(String numberOfPeople) {
		BooleanQuery subQuery = new BooleanQuery();
		Query queryTerm = null;

		// limite inferiore del range
		Double min = Utils.getTimesFromArray(
				Utils.getNumbersFromString(numberOfPeople)).get("min");
		// limite superiore del range
		Double max = Utils.getTimesFromArray(
				Utils.getNumbersFromString(numberOfPeople)).get("max");

		// reale limite superiore del range
		max = Index.getUpperBound(Index.NUMBER_OF_PEOPLE, max);
		// query sul valore minimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.min(Index.NUMBER_OF_PEOPLE), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.NUMBER_OF_PEOPLE));
		subQuery.add(queryTerm, Occur.SHOULD);
		// query sul valore massimo salvato nel documento
		queryTerm = NumericRangeQuery.newDoubleRange(
				Index.max(Index.NUMBER_OF_PEOPLE), 4, min, max, true, true);
		queryTerm.setBoost(Index.QUERY_BOOST.get(Index.NUMBER_OF_PEOPLE));
		subQuery.add(queryTerm, Occur.SHOULD);

		return subQuery;
	}// end-makeQueryNumberOfPeolpe

}
