package gfir.query.model;

import gfir.utils.Constant;

import java.util.ArrayList;

public class QuerySearch {
	
	//sequenza di parole nella casella di ricerca
	private String searchInput;
	//oggetti di tipo ingrediente
	private ArrayList<QueryItem> ingredients;
	//sequenza di parole separate da “ ” (e.g. “basso medio”)
	private String difficulty;
	//tempo di cottura espresso come range (e.g. “10-40”)
	private String cookTime;
	//tempo di preparazione espresso come range (e.g. “10-40”)
	private String prepareTime;
	//numero di persone espresso come range (e.g. “10-40”)
	private String numberOfPeople;
	//sequenza di parole separate da “-” (e.g. “basso-molto basso”)
	private String price;
	//categoria specifica sulla quale fare una ricerca
	private String category;
	//numero massimo di risultati
	private int maxResults;
	//hash delle ricette con un like
	private ArrayList<String> likes;
	
	
	/**
	 * costruttore di default
	 */
	public QuerySearch() {
		this.searchInput = null;
		this.ingredients = new ArrayList<QueryItem>();
		this.difficulty = null;
		this.cookTime = null;
		this.prepareTime = null;
		this.numberOfPeople = null;
		this.price = null;
		this.category = null;
		this.maxResults = Constant.MAX_SEARCH_RESULTS;
		this.likes = new ArrayList<String>();
	}


	/**
	 * costruttore con parametri
	 * @param searchInput
	 * @param ingredients
	 * @param difficulty
	 * @param otherFields
	 * @param price
	 */
	public QuerySearch(String searchInput, ArrayList<QueryItem> ingredients,
			String difficulty, String price, String prepareTime, String cookTime,
			String numberOfPeople, String category, int maxResults, 
			ArrayList<String> likes) {
		this.searchInput = searchInput;
		this.ingredients = ingredients;
		this.difficulty = difficulty;
		this.prepareTime = prepareTime;
		this.cookTime = cookTime;
		this.numberOfPeople = numberOfPeople;
		this.price = price;
		this.category = category;
		this.maxResults = maxResults;
		this.likes = likes;
	}


	/**
	 * @return the searchInput
	 */
	public String getSearchInput() {
		return searchInput;
	}


	/**
	 * @param searchInput the searchInput to set
	 */
	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}


	/**
	 * @return the ingredients
	 */
	public ArrayList<QueryItem> getIngredients() {
		return ingredients;
	}


	/**
	 * @param ingredients the ingredients to set
	 */
	public void setIngredients(ArrayList<QueryItem> ingredients) {
		this.ingredients = ingredients;
	}
	
	
	/**
	 * @return the difficulty
	 */
	public String getDifficulty() {
		return difficulty;
	}
	
	
	/**
	 * @return the difficulty
	 */
	public String[] getDifficulties() {
		return difficulty.split("\\s");
	}


	/**
	 * @param difficulty the difficulty to set
	 */
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}


	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	
	
	/**
	 * @return the price
	 */
	public String[] getPrices() {
		return this.price.split(" ");
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	
	
	/**
	 * @return the cookTime
	 */
	public String getCookTime() {
		return cookTime;
	}


	/**
	 * @param cookTime the cookTime to set
	 */
	public void setCookTime(String cookTime) {
		this.cookTime = cookTime;
	}


	/**
	 * @return the prepareTime
	 */
	public String getPrepareTime() {
		return prepareTime;
	}


	/**
	 * @param prepareTime the prepareTime to set
	 */
	public void setPrepareTime(String prepareTime) {
		this.prepareTime = prepareTime;
	}


	/**
	 * @return the numberOfPeople
	 */
	public String getNumberOfPeople() {
		return numberOfPeople;
	}


	/**
	 * @param numberOfPeople the numberOfPeople to set
	 */
	public void setNumberOfPeople(String numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}


	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}


	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	/**
	 * @return the maxResults
	 */
	public int getMaxResults() {
		return maxResults;
	}


	/**
	 * @param maxResults the maxResults to set
	 */
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}


	/**
	 * @return the likes
	 */
	public ArrayList<String> getLikes() {
		return likes;
	}


	/**
	 * @param likes the likes to set
	 */
	public void setLikes(ArrayList<String> likes) {
		this.likes = likes;
	}
	
}
