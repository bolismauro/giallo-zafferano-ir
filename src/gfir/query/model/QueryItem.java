package gfir.query.model;

public class QueryItem {
	
	/*
	 * nome dell'indice, nel caso particolare di un ingrediente 
	 * corrisponde al nome dell'ingrediente stesso
	 */
	private String name;
	/*
	 * valore associato all'indice, nel caso particolare di un
	 * ingrediente corrisponde al campo "amount"
	 */
	private String value;
	
	
	/**
	 * costruttore di default
	 */
	public QueryItem() {
		this.name = null;
		this.value = null;
	}


	/**
	 * costruttore tramite parametri 
	 * @param name
	 * @param value
	 */
	public QueryItem(String name, String value) {
		this.name = name;
		this.value = value;
	}


	/**
	 * @return nome dell'indice o dell'ingrediente
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name nome dell'indice o dell'ingrediente
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return valore associato all'indice o quantita' di un ingrediente
	 */
	public String getValue() {
		return value;
	}


	/**
	 * @param value valore associato all'indice o quantita' di un ingrediente
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
