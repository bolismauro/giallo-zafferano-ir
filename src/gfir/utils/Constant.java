package gfir.utils;

public class Constant {
	
	//Questo parametro abilita o disabilita la scrittura del log su file
	public static boolean FILE_LOG = true;
	//Questo parametro abilita o disabilita l'output del log su console (stdout)
	public static boolean CONSOLE_LOG = true;
	//Numero massimo di risultati di default ottenibili dalla ricerca
	public static int MAX_SEARCH_RESULTS = 20;
	
}
