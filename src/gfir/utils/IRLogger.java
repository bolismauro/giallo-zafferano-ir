package gfir.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class IRLogger {
	
	/**
	 * Istanza del logger condivisa
	 */
	private static Logger sharedLogger;
	
	/**
	 * Peremette di ottenere l'oggetto logger condiviso
	 * @return Logger r
	 */
	public static Logger getSharedLogger(){
		
		if (sharedLogger == null){
			sharedLogger = Logger.getRootLogger();
			
			if (Constant.CONSOLE_LOG && Constant.FILE_LOG){
				PropertyConfigurator.configure("log4j_both.properties");
			}else if (Constant.FILE_LOG) {
				PropertyConfigurator.configure("log4j_file.properties");
			}else {
				PropertyConfigurator.configure("log4j_console.properties");
			}
		}
		
		return sharedLogger;
		
	}
	
	
}
