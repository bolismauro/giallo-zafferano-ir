package gfir.utils;

import gfir.analyzer.AnalizzatoreCottura;
import gfir.analyzer.AnalizzatoreItaliano;
import gfir.analyzer.AnalizzatoreNote;
import gfir.analyzer.AnalizzatorePersone;
import gfir.analyzer.AnalizzatorePreparazione;
import gfir.analyzer.AnalizzatoreQuantitaIngrediente;
import gfir.analyzer.KeyWordAnalyzerMinuscolo;
import gfir.analyzer.StandardAnalizzatoreItaliano;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

/**
 * Indici usati per creare i documenti di lucene 
 * e relative caratteristiche a query time
 */
public class Index extends Constant {
	
	public static String HASH = "recipeHash";
	public static String TITLE = "title";
	public static String URL = "url";
	public static String IMG_URL = "imgUrl";
	public static String DIFFICULTY = "difficulty";
		public static String[] DIFFICULTY_VALUES = new String[] {"molto_basso","basso","medio","elevato","molto_elevato"};
	public static String COOK_TIME = "cookTime";
		//tempo di cottura massimo selezionabile da interfaccia
		public static Double MIN_COOK_TIME = (double) 0;
		//tempo di cottura massimo selezionabile da interfaccia
		public static Double MAX_COOK_TIME = (double) 180;
		//limite superiore per il tempo di cottura
		public static Double MAX_COOK_TIME_DEFAULT = (double) 1440;
	public static String PREPARE_TIME = "prepareTime";
		//tempo di cottura massimo selezionabile da interfaccia
		public static Double MIN_PREPARE_TIME = (double) 0;
		//tempo di preparazione massimo selezionabile da interfaccia
		public static Double MAX_PREPARE_TIME = (double) 180;
		//limite superiore per il tempo di preparazione
		public static Double MAX_PREPARE_TIME_DEFAULT = (double) 1440;
	public static String NUMBER_OF_PEOPLE = "numberOfPeople";
		//tempo di cottura massimo selezionabile da interfaccia
		public static Double MIN_NUMBER_OF_PEOPLE = (double) 0;
		//numero di persone massimo selezionabile da interfaccia
		public static Double MAX_NUMBER_OF_PEOPLE = (double) 10;
		//limite superiore per il numero di persone
		public static Double MAX_NUMBER_OF_PEOPLE_DEFAULT = (double) 100;
	public static String PRICE = "price";
		//valori possibili del campo price
		public static String[] PRICE_VALUES = new String[] {"molto_basso","basso","medio","elevato","molto_elevato"};
	public static String NOTE = "note";
	public static String SUMMARY = "summary";
	public static String PREPARATION = "preparation";
	public static String CATEGORY = "category";
	
	public static String INGREDIENT = "ingredient";
	public static String AMOUNT = "amount";
	public static String INGREDIENT_URL = "ingredientUrl";
	
	public static String DOC_TYPE = "docType";
	public static String DOC_RECIPE = "recipe";
	public static String DOC_INGREDIENT = "ingredient";
	
	public static float LIKE_BOOST = (float) 1.1;
	
	
	/**
	 * HashMap che associa ad ogni indice associa valori di default per il tipo di
	 * operatore da inserire nella query (AND=true, OR=false) e la distanza di Levenshtein
	 */
	public static final HashMap<String, Integer> QUERY_FEATURES;
    static
    {
        QUERY_FEATURES = new HashMap<String, Integer>();
        
        QUERY_FEATURES.put(TITLE, 1);
        QUERY_FEATURES.put(NOTE, 2);
    	QUERY_FEATURES.put(SUMMARY, 2);
    	QUERY_FEATURES.put(PREPARATION, 2);
    	QUERY_FEATURES.put(CATEGORY, 1);
        
        QUERY_FEATURES.put(DIFFICULTY, 0);
        
        QUERY_FEATURES.put(COOK_TIME, 0);
        QUERY_FEATURES.put(PREPARE_TIME, 0);
        QUERY_FEATURES.put(NUMBER_OF_PEOPLE, 0);
        QUERY_FEATURES.put(PRICE, 0);
        
        QUERY_FEATURES.put(INGREDIENT, 1);
        QUERY_FEATURES.put(AMOUNT, 2);
        
    }//end-QUERY_FEATURES
    
    
    /**
     * HashMap che associa ad ogni il boost a query-time
     */
    public static final HashMap<String, Float> INDEX_BOOST;
    static
    {
    	INDEX_BOOST = new HashMap<String, Float>();
        
    	INDEX_BOOST.put(TITLE, (float) 2.6);
    	INDEX_BOOST.put(NOTE, (float) 1);
    	INDEX_BOOST.put(SUMMARY, (float) 1.3);
    	INDEX_BOOST.put(PREPARATION, (float) 0.8);
    	INDEX_BOOST.put(CATEGORY, (float) 1);

    	INDEX_BOOST.put(DIFFICULTY, (float) 1);
    	INDEX_BOOST.put(COOK_TIME, (float) 1);
    	INDEX_BOOST.put(PREPARE_TIME, (float) 1);
    	INDEX_BOOST.put(NUMBER_OF_PEOPLE, (float) 1);
    	INDEX_BOOST.put(PRICE, (float) 1);
    	INDEX_BOOST.put(INGREDIENT, (float) 2.5);
    	INDEX_BOOST.put(AMOUNT, (float) 1.2);
        
    }//end-INDEX_BOOST
    
    
    /**
     * HashMap che associa ad ogni il boost a query-time
     */
    public static final HashMap<String, Float> QUERY_BOOST;
    static
    {
        QUERY_BOOST = new HashMap<String, Float>();
        
        QUERY_BOOST.put(TITLE, (float) 2.6);
        QUERY_BOOST.put(NOTE, (float) 1);
        QUERY_BOOST.put(SUMMARY, (float) 1.3);
        QUERY_BOOST.put(PREPARATION, (float) 0.8);
        QUERY_BOOST.put(CATEGORY, (float) 2.3);

        QUERY_BOOST.put(DIFFICULTY, (float) 1);
        QUERY_BOOST.put(COOK_TIME, (float) 1);
        QUERY_BOOST.put(PREPARE_TIME, (float) 1);
        QUERY_BOOST.put(NUMBER_OF_PEOPLE, (float) 1);
        QUERY_BOOST.put(PRICE, (float) 1);
        QUERY_BOOST.put(INGREDIENT, (float) 2.1);
        QUERY_BOOST.put(AMOUNT, (float) 1.2);
        
    }//end-QUERY_BOOST
    
    
    /**
	 * HashMap che associa ad ogni indice associa valori di default per il tipo di
	 * operatore da inserire nella query (AND=true, OR=false) e la distanza di Levenshtein
	 */
	public static final HashMap<String, Analyzer> INDEX_ANALYZER;
    static
    {
    	INDEX_ANALYZER = new HashMap<String, Analyzer>();
        
    	INDEX_ANALYZER.put(TITLE, new StandardAnalizzatoreItaliano());
		INDEX_ANALYZER.put(DIFFICULTY, new KeyWordAnalyzerMinuscolo());
		INDEX_ANALYZER.put(COOK_TIME, new AnalizzatoreCottura());
		INDEX_ANALYZER.put(PREPARE_TIME, new AnalizzatorePreparazione());
		INDEX_ANALYZER.put(NUMBER_OF_PEOPLE, new AnalizzatorePersone());
		INDEX_ANALYZER.put(PRICE, new KeyWordAnalyzerMinuscolo());
		INDEX_ANALYZER.put(NOTE, new AnalizzatoreNote());
		INDEX_ANALYZER.put(SUMMARY, new AnalizzatoreItaliano());
		INDEX_ANALYZER.put(PREPARATION, new AnalizzatoreItaliano());
		INDEX_ANALYZER.put(INGREDIENT, new KeyWordAnalyzerMinuscolo());
		INDEX_ANALYZER.put(AMOUNT, new AnalizzatoreQuantitaIngrediente());
		INDEX_ANALYZER.put(CATEGORY, new StandardAnalizzatoreItaliano());
		
		//analizzatorore per i campi numerici
		INDEX_ANALYZER.put(min(COOK_TIME), new StandardAnalyzer(Version.LUCENE_40));
		INDEX_ANALYZER.put(max(COOK_TIME), new StandardAnalyzer(Version.LUCENE_40));
		INDEX_ANALYZER.put(min(PREPARE_TIME), new StandardAnalyzer(Version.LUCENE_40));
		INDEX_ANALYZER.put(max(PREPARE_TIME), new StandardAnalyzer(Version.LUCENE_40));
		INDEX_ANALYZER.put(min(NUMBER_OF_PEOPLE), new StandardAnalyzer(Version.LUCENE_40));
		INDEX_ANALYZER.put(max(NUMBER_OF_PEOPLE), new StandardAnalyzer(Version.LUCENE_40));
        
    }//end-INDEX_ANALYZER
    
    
    /**
     * elenco di indici collegati a valori numerici
     */
    public static final ArrayList<String> NUMERICAL_INDECES;
    static {
    	NUMERICAL_INDECES = new ArrayList<String>();
    	NUMERICAL_INDECES.add(COOK_TIME);
    	NUMERICAL_INDECES.add(PREPARE_TIME);
    	NUMERICAL_INDECES.add(NUMBER_OF_PEOPLE);
    }//end-NUMERICAL_INDECES
    
    
    /**
     * elenco di indici relativi ad un ingrediente
     */
    public static final ArrayList<String> INGREDIENT_INDECES;
    static {
    	INGREDIENT_INDECES = new ArrayList<String>();
    	INGREDIENT_INDECES.add(INGREDIENT);
    	INGREDIENT_INDECES.add(AMOUNT);
    }//end-INGREDIENT_INDECES
    
    
    /**
     * elenco di indici sui quali l'utente puo' fare query libere (input form)
     */
    public static final ArrayList<String> SEARCH_INPUT;
    static {
    	SEARCH_INPUT = new ArrayList<String>();
    	SEARCH_INPUT.add(TITLE);
    	SEARCH_INPUT.add(NOTE);
    	SEARCH_INPUT.add(SUMMARY);
    	SEARCH_INPUT.add(PREPARATION);
    	SEARCH_INPUT.add(CATEGORY);
    	//SEARCH_INPUT.add(INGREDIENT);
    }//end-SEARCH_INPUT
    
    
    /**
     * @param index
     * @return dato il nome dell'indice lo ritorna con il prefisso 'min'
     */
    public static final String min(String index) {
    	return "min" + Character.toUpperCase(index.charAt(0)) + index.substring(1, index.length());
    }//end-min
    
    
    /**
     * @param index
     * @return dato il nome dell'indice lo ritorna con il prefisso 'max'
     */
    public static final String max(String index) {
    	return "max" + Character.toUpperCase(index.charAt(0)) + index.substring(1, index.length());
    }//end-max
    
    
    /**
     * dato il valore massimo selezionato dall'utente, restituisce il reale limite 
     * superiore per effettuare la query
     * @param index
     * @param max
     * @return
     */
    public static final Double getUpperBound(String index, Double max) {
    	
    	if( index.equals(COOK_TIME) && max.compareTo(MAX_COOK_TIME) == 0 ) {
    		return MAX_COOK_TIME_DEFAULT;
    	} else if( index.equals(PREPARE_TIME) && max.compareTo(MAX_PREPARE_TIME) == 0 ) {
    		return MAX_PREPARE_TIME_DEFAULT;
    	} else if( index.equals(NUMBER_OF_PEOPLE) && max.compareTo(MAX_NUMBER_OF_PEOPLE) == 0 ) {
    		return MAX_NUMBER_OF_PEOPLE_DEFAULT;
    	} else {
			return max;
		}
    	
    }//end-getUpperBound
    
    
    /**
     * dati i valori minimo e massimo selezionati dall'utente, se questi sono gli estremi
     * la valutazione sull'indice non deve essere fatta
     * @param index
     * @param max
     * @return
     */
    public static final Boolean checkBounds(String index, Double min, Double max) {
    	
    	if( index.equals(COOK_TIME) && 
    		max.compareTo(MAX_COOK_TIME) == 0 && 
    		min.compareTo(MIN_COOK_TIME) == 0 ) {
    		return false;
    	} else if( index.equals(PREPARE_TIME) && 
    				max.compareTo(MAX_PREPARE_TIME) == 0 && 
    				min.compareTo(MIN_PREPARE_TIME) == 0 ) {
    		return false;
    	} else if( index.equals(NUMBER_OF_PEOPLE) && 
    				max.compareTo(MAX_NUMBER_OF_PEOPLE) == 0 && 
    				min.compareTo(MIN_NUMBER_OF_PEOPLE) == 0 ) {
    		return false;
    	} else {
			return true;
		}
    	
    }//end-checkBounds
	
}
