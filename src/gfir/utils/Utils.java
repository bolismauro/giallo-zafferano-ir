package gfir.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	
	/**
	 * data l'url di un nodo ne deduce l'etichetta
	 * @param url
	 */
	public static String getLabelFromUrl(URI url) {
		
		//array di stringhe temporaneo per la manipolazione dell'url
		String[] tmp;
		
		try {
		
			if( url.getPath().endsWith("/") ) {
				tmp = url.getPath().split("/");
				//la stringa e' diversa dal solo carattere '/'
				if( tmp.length > 1 ) {
					return tmp[ tmp.length-1 ].replace("-", " ");
				}
				//la stringa e' il solo carattere '/'
				else {
					return "";
				}
			}//end-if
			
			else {
				tmp = url.getPath().split("/");
				return tmp[ tmp.length-1 ].replace(".html", "").replace("-", " ");
			}//end-else
			
		} catch(Exception e) {
			return null;
		}
		
	}//end-setLabelFromUrl
	
	
	/**
	 * @param url
	 * @return hash code dell'etichetta associata ad un url
	 */
	public static Integer getHashFromUrl(URI url) {
		String id = getLabelFromUrl(url) + "@" + getHostName(url);
		return id.hashCode();
	}//end-getHashFromUrl
	
	
	/**
	 * @param url
	 * @return nominio di un generico url
	 */
	public static String getDomainFromUrl(URI url) {
		//dominio dell'url fornito
		String domain = "";
		
		//array di domini letti dalla lista
		ArrayList<String> domains = new ArrayList<String>();
		
		//lettura del file contenente la lista dei domini
		InputStream f = Utils.class.getResourceAsStream("suffixDomains");
		Reader r;
		try {
			r = new InputStreamReader(f, "utf-8");
			BufferedReader br = new BufferedReader(r);
			//stringa contenente la singola linea del file
		    String line;
		    //popolamento dell'array partendo dal file
		    while ((line = br.readLine()) != null) {
		    	//vengono saltate le linee commentate
				if( !line.startsWith("//") ) {
					domains.add( line.trim() );
				}
			}//end-while
		    
		    br.close();
			
		} catch (IOException e) {
			//e.printStackTrace();
			IRLogger.getSharedLogger().fatal("Errore lettura suffixDomains file");
		}
		
		//host dell'url fornito
		String s = url.getHost();
		//array di elementi della stringa host (elementi separati da ".")
	    String[] arr = s.split("\\.");
	    
	    //flag per uscire dal ciclo
	    boolean flag = false;
	    
	    //iteratore lungo l'array di elementi dell'host
	    int j = 0;
	    
	    //ricerca del dominio dell'url
	    while(!flag) {
	    	
	    	domain = "";
	    	
	    	//elemento dell'host dal quale cominciare a valutare la stringa
	    	int i = j;
	    	//concatenazione dei vari elementi
		    for(; i<arr.length-1; i++) {
		    	domain += arr[i] + "."; 
		    }
		    domain += arr[i];
		    
		    //se la stringa domain e' contenuta nella lista di domini esisteni
		    //allora il dominio del sito e' stato trovato
			if( domains.contains( domain ) ) {
				break;
			}
			
			//modifica dell'elemento iniziale per la ricerca del dominio
			j++;
			
			//nel caso in cui il dominio non sia stato trovato e si sia considerato 
			//anche l'ultimo singolo elemento dell'host, allora l'url non ha dominio
			if( j >= arr.length ) {
				flag = true;
				domain = "";
			}
		
	    }//end-while
		
		return domain;
	}//end-getDomainFromUrl
	
	
	/**
	 * @param url
	 * @return nome dell'host
	 * e.g. str1.str.str3.dominio => str3 
	 */
	public static String getHostName(URI url) {
		
		//dominio dell'url fornito
		String domain = "." + getDomainFromUrl(url);
		
		//host dell'url fornito
		String host = url.getHost();
		
		//indice finale della stringa da tornare, delimitato dal dominio 
		int end = host.lastIndexOf(domain)-1;
		//indice iniziale della stringa da tornare, delimitato dal primo 
		//"." che precede il dominio
		int begin = host.lastIndexOf(".",end)+1;
		
		return host.substring(begin, end+1);
	}//end-getHostName
	
	
	/**
	 * data una stringa riconosce la presenza di valori numerici restituendoli in una lista
	 * @param text
	 * @return
	 */
	public static ArrayList<Double> getNumbersFromString(String text) {
		ArrayList<Double> numbers = new ArrayList<Double>();
		
		//pattern da riconoscere
		Pattern p = Pattern.compile("[0-9]+");
		//matching all'interno della stringa
		Matcher m = p.matcher(text);
		//creazione di una lista con i valori trovati
		while (m.find()) {
		    numbers.add( Double.parseDouble(m.group()) );
		}
		
		return numbers;
	}//end-getNumbersFromString
	
	
	/**
	 * metodo che permette di associare le tempistiche di cottura/preparazione in base
	 * ai valori numerici presenti nelle stringhe
	 * @param numbers
	 * @return HashMap<String,Integer> in cui 'max' e' l'indice al valore piu' grande e
	 * 'min' a quello piu' piccolo; in caso di valore unico 'min' = 'max'  
	 */
	public static HashMap<String, Double> getTimesFromArray(ArrayList<Double> numbers) {
		//hasmap contenente i valori minimi e massimi dei tempi di cottura o preparazione
		HashMap<String, Double> tempi = new HashMap<String, Double>();
		//valore minimo
		Double min = 0.0;
		//valore massimo
		Double max = 0.0;
		
		//caso in cui sia presente un range di valori (min, max)
		if(numbers.size() > 1) {
			min = numbers.get(0);
			max = numbers.get(1);
		}
		//caso in cui il valore sia unico
		else {
			min = max = numbers.get(0);
		}
		
		tempi.put("min", min);
		tempi.put("max", max);
		
		return tempi;
	}//end-getNumbersFromString
	
	
}
