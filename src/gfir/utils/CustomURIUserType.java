package gfir.utils;
/**
 * Custom usertype per la classe url,
 * ci permette di visualizzare nel database l'url
 */

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
 
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
 
public class CustomURIUserType implements UserType {
   
    private static final int[] SQL_TYPES = {Types.VARCHAR};
    
    public int[] sqlTypes() {
        return SQL_TYPES;
    }
 
    public Class returnedClass() {
        return String.class;
    }
 
    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner)
                             throws HibernateException, SQLException {
    	 String urlStr = resultSet.getString(names[0]);
    	 try {
			return new URI(urlStr);
		} catch (URISyntaxException e) {
			IRLogger.getSharedLogger().error("Errore mentre creo URI");
			return null;
		} catch (Exception e) {
			IRLogger.getSharedLogger().error("Errore generico mentre creo URI");
			return null;
		}
    }
 
   public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index) 
                          throws HibernateException, SQLException {
        if (null == value) {
            preparedStatement.setNull(index, Types.VARCHAR);
        } else {
        	URI uri = (URI) value;
        	String strUri = uri.toString();
            preparedStatement.setString(index, strUri);
        }
    }
 
    public Object deepCopy(Object value) throws HibernateException{
        return value;
    }
 
    public boolean isMutable() {
        return false;
    }
 
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }
 
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)value;
    }
 
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
    
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }
    
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y)
            return true;
        if (null == x || null == y)
            return false;
        return x.equals(y);
    }
}