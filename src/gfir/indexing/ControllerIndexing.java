package gfir.indexing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.PerFieldSimilarityWrapper;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import gfir.analyzer.AnalizzatoreCottura;
import gfir.analyzer.AnalizzatoreItaliano;
import gfir.analyzer.AnalizzatoreNote;
import gfir.analyzer.AnalizzatorePersone;
import gfir.analyzer.AnalizzatorePreparazione;
import gfir.analyzer.AnalizzatoreQuantitaIngrediente;
import gfir.analyzer.KeyWordAnalyzerMinuscolo;
import gfir.analyzer.StandardAnalizzatoreItaliano;
import gfir.crawler.HibernateDataStore;
import gfir.utils.IRLogger;
import gfir.utils.Index;
/**
 * La classe permette di eseguire l'indicizzazione in moddalita' mono o multi thread, 
 * garantendo la sincronizzazione tra i thread.
 * @author Seba
 *
 */
public class ControllerIndexing {
	
	//path 
	private final String path = "WebContent/gialloZafferanoIndex";
	
	// numero di thread per indicizzare
	private final int N_INDICI=40;
	
	
	public void indicizza() throws IOException{
		
		//creazione datastore
		HibernateDataStore dataStore= new HibernateDataStore();
		
		//creazione percorso
		File indexFolder1 = new File(path);
		
		//creazione analizzatore
		Analyzer analyzer1= getAnalyzer();
		//apertura indice
		IndexWriter index = getIndexWriter(indexFolder1, analyzer1);
		
		//lista thread per sincronizzarare la chiusura dell'indice
		ArrayList<Thread> indici = new ArrayList<Thread>();
		
		//creazione e partenza thread
		for(int i=0; i < N_INDICI; i++){
		Indexing thread =new Indexing(dataStore, index);
		Thread t = new Thread(thread);
		t.start();
		indici.add(t);
		}
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//sincronizzazione per chiusura indice
		for(Thread t : indici){
			try {
				t.join();
			} catch (InterruptedException e) {
				IRLogger.getSharedLogger().info("Errore join");
			}
		}
	
		//chiusura indice
		index.close();
		
		IRLogger.getSharedLogger().info("Indicizzazione finita con successo");
	}
	
	/**
	 * 
	 * @param folder cartella indice
	 * @param analyzer tipo analizzatore
	 * @return IndexWriter
	 * @throws IOException
	 */
	private IndexWriter getIndexWriter(File folder, Analyzer analyzer)
			throws IOException {
		
		// Crazione Lucene index
		Directory directory = FSDirectory.open(folder);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
				analyzer);
		
		// Modalit� CREATE singolo thread o CREATE_OR_APPEND per multi thread 
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
		
		//Similarit� di default
		config.setSimilarity( new DefaultSimilarity());
		return new IndexWriter(directory, config);
	}
/**
 * Il metodo crea un analizzatore personalizzato per ogni campo
 * @return Analyzer 
 */
	private Analyzer getAnalyzer(){
		Analyzer analyzerBase = new StandardAnalizzatoreItaliano();
		HashMap<String, Analyzer> aMap = Index.INDEX_ANALYZER;
		Analyzer analyzer1 = new PerFieldAnalyzerWrapper(analyzerBase, aMap);
		
		return analyzer1;
	}
}
