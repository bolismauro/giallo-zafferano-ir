package gfir.indexing;

import java.io.IOException;

import gfir.crawler.HibernateDataStore;
/**
 * La classe permette di eseguire un'inidicizzazione in locale tramite la chiamata a metodo indicizza di ControllerIndexing.
 * @author Seba
 *
 */
public class RunIndicizzazione {
	
	public static void main(String[] args)throws IOException{
		
		//chimata al metodo della classe Controller per inizializzare i thread, iniziare l'indicizzazione e sincronizzare la chiusura dell'indice
		new ControllerIndexing().indicizza();
	}

}
