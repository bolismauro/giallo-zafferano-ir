package gfir.indexing;

import java.io.IOException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoubleField;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import gfir.crawler.interfaces.DataStore;
import gfir.crawler.interfaces.Indexer;
import gfir.model.GialloZafferanoRecipe;
import gfir.model.Ingredient;
import gfir.utils.IRLogger;
import gfir.utils.Index;
import gfir.utils.Utils;
/**
 * La classe permette di indicizzare in locale le pagine di tipo Recipe prese da un datastore.La classe implementa runnable quindi � possibile eseguirla mono o multi thread 
 * Lastruttura dell'indice e' a segmento.
 * @author Seba
 *
 */
public class Indexing extends Indexer {

	//Variabile per contate il numero di pagine indicizzate
	private int i=0;

	public Indexing(DataStore dataStore, IndexWriter index) {
		super(dataStore, index);
	}

	public void run() {
	
			
			//  Lista di documenti da inserire nel segmento
			List<Document> docs = new ArrayList<Document>();
			
			//Recupero una pagina dal datastore
			GialloZafferanoRecipe pagina = (GialloZafferanoRecipe) dataStore.getNextRecipe();
			
			//Chiusura thread quando non ci sono piu' pagine da analizzare
			while (pagina != null) {

				
				//Creazione documento ed estrazione dati da oggetto pagina
				Document d1 = new Document();
				
				//identificativo della ricetta
				Field hash = new StringField(Index.HASH, Integer.toString( pagina.getRecipeHash() ), Field.Store.YES);
				d1.add(hash);
				
				if (pagina.getTitle() != null) {
					Field title = new TextField(Index.TITLE, pagina.getTitle(),Field.Store.YES);
					title.setBoost(Index.INDEX_BOOST.get(Index.TITLE));
					d1.add(title);
				}

				if (pagina.getUrl() != null) {
					Field url = new StringField(Index.URL, pagina.getUrl().toString(), Field.Store.YES);
					d1.add(url);
				}

				if (pagina.getImgUrl() != null) {
					Field imgUrl = new StringField(Index.IMG_URL, pagina.getImgUrl().toString(), Field.Store.YES);
					d1.add(imgUrl);
				}

				if (pagina.getDifficulty() != null) {
					Field difficulty = new TextField(Index.DIFFICULTY,pagina.getDifficulty(), Field.Store.YES);
					difficulty.setBoost(Index.INDEX_BOOST.get(Index.DIFFICULTY));
					d1.add(difficulty);
				}

				if (pagina.getCookTime() != null) {
					Field cookTime = new TextField(Index.COOK_TIME,pagina.getCookTime(), Field.Store.YES);
					cookTime.setBoost(Index.INDEX_BOOST.get(Index.COOK_TIME));
					d1.add(cookTime);
					//estrazione del tempo minimo e massimo di cottura
					HashMap<String,Double> cookTimeRange = Utils.getTimesFromArray( Utils.getNumbersFromString(pagina.getCookTime()) );
					Field minCookTime = new DoubleField(Index.min(Index.COOK_TIME), cookTimeRange.get("min"), Field.Store.YES);
					minCookTime.setBoost(Index.INDEX_BOOST.get(Index.COOK_TIME));
					Field maxCookTime = new DoubleField(Index.max(Index.COOK_TIME), cookTimeRange.get("max"), Field.Store.YES);
					maxCookTime.setBoost(Index.INDEX_BOOST.get(Index.COOK_TIME));
					d1.add(minCookTime);
					d1.add(maxCookTime);
				}

				if (pagina.getPrepareTime() != null) {
					Field prepareTime = new TextField(Index.PREPARE_TIME,pagina.getPrepareTime(), Field.Store.YES);
					prepareTime.setBoost(Index.INDEX_BOOST.get(Index.PREPARE_TIME));
					d1.add(prepareTime);
					//estrazione del tempo minimo e massimo di preparazione
					HashMap<String,Double> prepareTimeRange = Utils.getTimesFromArray( Utils.getNumbersFromString(pagina.getPrepareTime()) );
					Field minPrepareTime = new DoubleField(Index.min(Index.PREPARE_TIME), prepareTimeRange.get("min"), Field.Store.YES);
					minPrepareTime.setBoost(Index.INDEX_BOOST.get(Index.PREPARE_TIME));
					Field maxPrepareTime = new DoubleField(Index.max(Index.PREPARE_TIME), prepareTimeRange.get("max"), Field.Store.YES);
					maxPrepareTime.setBoost(Index.INDEX_BOOST.get(Index.PREPARE_TIME));
					d1.add(minPrepareTime);
					d1.add(maxPrepareTime);
				}

				if (pagina.getNumberOfPeople() != null) {
					Field numberOfPeople = new TextField(Index.NUMBER_OF_PEOPLE, pagina.getNumberOfPeople(), Field.Store.YES);
					numberOfPeople.setBoost(Index.INDEX_BOOST.get(Index.NUMBER_OF_PEOPLE));
					d1.add(numberOfPeople);
					//se le dosi sono espresse per persone, si estrai il numero
					if( pagina.getNumberOfPeople().contains("persone") || pagina.getNumberOfPeople().contains("persona") ) {
						try {
							HashMap<String,Double> numPeoleRange = Utils.getTimesFromArray( Utils.getNumbersFromString(pagina.getNumberOfPeople()) );
							Field minNumberOfPeople = new DoubleField(Index.min(Index.NUMBER_OF_PEOPLE), numPeoleRange.get("min"), Field.Store.YES);
							minNumberOfPeople.setBoost(Index.INDEX_BOOST.get(Index.NUMBER_OF_PEOPLE));
							Field maxNumberOfPeople = new DoubleField(Index.max(Index.NUMBER_OF_PEOPLE), numPeoleRange.get("max"), Field.Store.YES);
							maxNumberOfPeople.setBoost(Index.INDEX_BOOST.get(Index.NUMBER_OF_PEOPLE));
							d1.add(minNumberOfPeople);
							d1.add(maxNumberOfPeople);
						} catch(Exception e) {
							IRLogger.getSharedLogger().error("Impossibile trovare numero persone ("+pagina.getNumberOfPeople()+")");
						}
					}//end-if
				}

				if (pagina.getPrice() != null) {
					Field price = new TextField(Index.PRICE, pagina.getPrice(),Field.Store.YES);
					price.setBoost(Index.INDEX_BOOST.get(Index.PRICE));
					d1.add(price);
				}

				if (pagina.getNote() != null) {
					Field note = new TextField(Index.NOTE, pagina.getNote(),Field.Store.YES);
					note.setBoost(Index.INDEX_BOOST.get(Index.NOTE));
					d1.add(note);
				}

				if (pagina.getSummary() != null) {
					Field summary = new TextField(Index.SUMMARY,pagina.getSummary(), Field.Store.YES);
					summary.setBoost(Index.INDEX_BOOST.get(Index.SUMMARY));
					d1.add(summary);
				}

				if (pagina.getPreparation() != null) {
					Field preparation = new TextField(Index.PREPARATION,pagina.getPreparation(), Field.Store.YES);
					preparation.setBoost(Index.INDEX_BOOST.get(Index.PREPARATION));
					d1.add(preparation);
				}

				
				//Tipo del documento, per distinguere tra ricette e ingredienti 
				Field type = new StringField(Index.DOC_TYPE, Index.DOC_RECIPE, Field.Store.YES);
				d1.add(type);

				
				//Campo categoria
				List<String> categoriaList = pagina.getCategories();

				if (categoriaList != null) {
					for (String c : categoriaList) {
						if (c != null) {
							Field category = new StringField(Index.CATEGORY,c.toString(), Field.Store.YES);
							category.setBoost(Index.INDEX_BOOST.get(Index.CATEGORY));
							d1.add(category);
						}
					}
				}

				//Lista classe ingredienti
				List<Ingredient> ingredienti = pagina.getIngredients();
				
				//Pulisco la lista per ogni documento pagina
				docs.clear();

				//Per ogni ingrediente viene creato un documento ingrediente 
				for (Ingredient e : ingredienti) {

					Document doc = new Document();
					
					//Tipo del documento, per distinguere tra ricette e ingredienti 
					type = new StringField(Index.DOC_TYPE, Index.DOC_INGREDIENT, Field.Store.YES);
					d1.add(type);
					
					//viene aggiunto l'identificativo della ricetta per legare ogni ingrediente ad essa
					doc.add(hash);
					
					if (e.getName() != null) {
						Field name = new TextField(Index.INGREDIENT, e.getName(),Field.Store.YES);
						name.setBoost(Index.INDEX_BOOST.get(Index.INGREDIENT));
						doc.add(name);
					}

					if (e.getAmount() != null) {
						Field amount = new TextField(Index.AMOUNT, e.getAmount(),Field.Store.YES);
						amount.setBoost(Index.INDEX_BOOST.get(Index.AMOUNT));
						doc.add(amount);
					}
					
					if (e.getAmount() != null) {
						Field ingredientUrl = new StringField(Index.INGREDIENT_URL, e.getUrl().toString(),Field.Store.YES);
						doc.add(ingredientUrl);
					}
					
					//Aggiungo il Documento ingrediente alla lista
					docs.add(doc);
				}
				
				//Aggiungo alla lista il Documento pagina con l'Id
				docs.add(d1);

				
				//Indicizzo l'intera lista di documenti in una struttura segmento
				try {
					
					index.addDocuments(docs);
					index.commit();
				
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				
				//output debug
				 
				IRLogger.getSharedLogger().info("Indicizzato: "+ i + " documenti" );
				i=i+1;
				
				//Richiesta pagina datastore per condizione ciclo
				pagina = (GialloZafferanoRecipe) dataStore.getNextRecipe();

			}
	}//end-run
	
}
