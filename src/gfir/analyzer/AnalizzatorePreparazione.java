package gfir.analyzer;

import java.io.Reader;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.apache.lucene.util.Version;

public class AnalizzatorePreparazione extends Analyzer{

private final static CharArraySet STOPWORD_CUSTOM=new CharArraySet(Version.LUCENE_40,Arrays.asList("preparazione"),true);
	
	
/**
 * Creates a
 * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
 * che tokenizza il testo presente in  {@link Reader}.
 * 
 * @return A
 *         {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
 *         costruito con {@link StandardTokenizer} filtrato con
 *         {@link LowerCaseFilter}, {@link StopFilter}
 */
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
		
		//tipo do tokenizzatore
		Tokenizer token =new StandardTokenizer(Version.LUCENE_40, reader);
		
		
		//Filtri in cascata
		TokenStream filtro=new StopFilter(Version.LUCENE_40,token,STOPWORD_CUSTOM);
		filtro= new LowerCaseFilter(Version.LUCENE_40,filtro);
		
		
		TokenStreamComponents tc= new TokenStreamComponents(token, filtro);
		
		return tc;
	}


}