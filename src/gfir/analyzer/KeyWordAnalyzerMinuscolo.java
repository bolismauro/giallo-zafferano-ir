package gfir.analyzer;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.core.KeywordTokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.apache.lucene.util.Version;

/**
 * 
 * Rende minuscolo il testo del token
 *
 */
public class KeyWordAnalyzerMinuscolo extends Analyzer{
	public KeyWordAnalyzerMinuscolo(){}

	 /**
	   * Creates a
	   * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
	   * che tokenizza il testo presente in  {@link Reader}.
	   * 
	   * @return A
	   *         {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
	   *         costruito con {@link KeywordTokenizer} filtrato con
	   *         {@link LowerCaseFilter}
	   */
	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
		
		/**
		 * Tokenizzazione delle parole
		 */
		Tokenizer token =new KeywordTokenizer(reader);
		
		/**
		 * Post-tonizzazione rende tutto minuscolo
		 */
		TokenStream filtro= new LowerCaseFilter(Version.LUCENE_40,token);
		
		TokenStreamComponents ts= new TokenStreamComponents(token, filtro);
		
		return ts;
	}
	

}
