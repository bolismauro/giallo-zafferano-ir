package gfir.analyzer;


import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.it.ItalianLightStemFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.analysis.util.WordlistLoader;
import org.apache.lucene.util.IOUtils;
import org.apache.lucene.util.Version;
import org.tartarus.snowball.ext.ItalianStemmer;

/**
 * La classe si occupa di definire un analizzatore per la lingua italiana
 */
public final class AnalizzatoreItaliano extends StopwordAnalyzerBase {
  
  /** File contenente le stopword. */
  public final static String DEFAULT_STOPWORD_FILE = "italian_stop.txt";
  
  /**
   * ArraySet contenente gli articoli
   */
  private static final CharArraySet DEFAULT_ARTICLES = CharArraySet.unmodifiableSet(
      new CharArraySet(Version.LUCENE_40, 
    		  Arrays.asList(
    		          "c", "l", "all", "dall", "dell", "nell", "sull", "coll", "pell", 
    		          "gl", "agl", "dagl", "degl", "negl", "sugl", "un", "m", "t", "s", "v", "d"
    		          ), true));

 
  /**
   * Caricamento DEFAULT_STOP_SET;
   */
  private static class DefaultSetHolder {
    static final CharArraySet DEFAULT_STOP_SET;

    static {
      try {
        DEFAULT_STOP_SET = WordlistLoader.getSnowballWordSet(IOUtils.getDecodingReader(SnowballFilter.class, 
            DEFAULT_STOPWORD_FILE, IOUtils.CHARSET_UTF_8), Version.LUCENE_40);
      } catch (IOException ex) {
       
        throw new RuntimeException("Non riesco a caricare le stopword");
      }
    }
  }

  /**
   * Costruttore con le default stop words
  */
  public AnalizzatoreItaliano() {
    super(Version.LUCENE_40, DefaultSetHolder.DEFAULT_STOP_SET);
  }
  

  /**
   * Creates a
   * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
   * che tokenizza il testo presente in  {@link Reader}.
   * 
   * @return A
   *         {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
   *         costruito con {@link StandardTokenizer} filtrato con
   *         {@link StandardFilter}, {@link ElisionFilter}, {@link LowerCaseFilter}, {@link StopFilter},{@link LengthFilter}
   */
  @Override
  protected TokenStreamComponents createComponents(String fieldName,
      Reader reader) {
	
	/**
	 * Tokenizzazione delle parole
	 */
    final Tokenizer source = new StandardTokenizer(Version.LUCENE_40, reader);
    TokenStream result = new StandardFilter(Version.LUCENE_40, source);
    
    /**
	 * Post-tonizzazione rende tutto minuscolo e toglie le stopword
	 */
    result = new ElisionFilter(result, DEFAULT_ARTICLES);
    result = new LowerCaseFilter(Version.LUCENE_40, result);
    result = new StopFilter(Version.LUCENE_40, result, stopwords);
    
    /**
     * Analizzatori per lo stemming opzionali
     */
   
      //result = new ItalianLightStemFilter(result);
      
      //result = new SnowballFilter(result, new ItalianStemmer());
    
      /**
       * Analizzatore che mantiene solo parole di lunghezza media
       */
      result=new LengthFilter(false, result, 3,8);
      return new TokenStreamComponents(source, result);
  }
  
  /**
   *  @return B
   *  {@link org.apache.lucene.analysis.charfilter} costruito con
   *  {@link HTMLStripCharFilter}
   */
  protected Reader initReader(String fieldName, Reader reader){
		
	  return new HTMLStripCharFilter(reader);
	}

}
