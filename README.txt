Per avviare il progetto è necessario seguire i seguenti passaggi

1_ Importare il progetto in Eclipse (Il gruppo ha utilizzato WTP)
2_ Modificare il file hibernate.cfg.xml inserendo i dati di un server con database mysql
3_ Per avviare il crawling eseguire il main contenuto in gfir.HibernateSchemaGenerator e poi gfir.RunCrawling
4_ Per avviare l'indicizzazione eseguire il main contenuto in gfir.indexing.RunIndicizzazione
5_ Per avviare l'interfaccia (nel progetto è gia presente un indice di lucene) avviare apache tomcat